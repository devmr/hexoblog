---
title: FFMPEG - Snippets - trucs et astuces
tags:
  - ffmpeg
categories:
  - [Tips-Tricks]
permalink: ffmpeg-tips-tricks-snippets-fr
date: 2019-08-13 20:40:07
---




**[FFMPEG](https://ffmpeg.org/)** est un utilitaire en ligne de commande et permet de manipuler c'est-à-dire : *convertir le format, extraire une bande son, extraire une/plusieurs images*... d'un fichier audio ou vidéo. Il est multiplateforme. Son installation est simple et rapide.

Voici quelques astuces que j'utilise souvent pour :

# Convertir un fichier .webm en .mp3

```
ffmpeg -i "Metallica.webm" -ab 160k -ar 44100 "Metallica.mp3"
```

# Convertir un fichier .mp4 en .avi

```
ffmpeg -i "Metallica.mp4" -f avi -vcodec libxvid -acodec mp3 -b:v 1700k -b:a 192k "Metallica.avi"
```

## ... inversement - convertir un fichier .avi en .mp4

```
ffmpeg -i "Metallica.AVI"  -c:v libx264 -pix_fmt yuv420p "Metallica.mp4"
```

# ... ou convertir plusieurs fichiers .avi en .mp4

Il s'agit d'une commande **shell** pour convertir par exemple plusieurs fichiers **.avi** dans un dossier en format **.mp4**

-  Se positionner dans un dossier en ligne de commande
- Puis lancer

```
cd folder
for file in $(ls | grep ".AVI"); do ffmpeg -i ${file} -c:v libx264 -pix_fmt yuv420p ${file}.MP4; done
```

