---
title: Featherjs  - Upload de fichier simple
tags:
  - featherjs
  - nodejs
  - multer
  - voca
  - sequelize
categories:
  - [featherjs]
  - [sequelize]
permalink: featherjs-upload-fr
date: 2019-07-13 00:40:07
---


Dans l'article précédent:  {% post_link featherjs-authentication-upload-fr 'Featherjs - authentification avec token' %} , nous avons abordé :		

- L'authentification d'un utilisateur  via jwt
- La protection d'une ressource: URL

... en créant un service `event` . Dans cet article, nous allons toujours utiliser le service `event` et  présenterons l’envoi de fichier simple depuis la machine vers le serveur.

# Installation de librairies utiles

Installons 2 librairies javascript/nodejs suivantes via `npm`:

```
npm install multer voca --save
```

**[Multer](https://github.com/expressjs/multer)** : middleware nodejs qui permet d'envoyer un fichier depuis l'ordinateur vers un serveur.

**[Voca](https://vocajs.com/)**: librairie qui possède plusieurs fonctions de manipulation de chaines de caractères. Dans notre cas, elle sera utile dans le cas d'une "slugification" (*conversion de texte en URL*). Exemple: le texte "**Je suis spécialiste featherjs**" sera converti en "**je-suis-specialiste-featherjs**"

## Configuration de multer

Nous allons stocker les fichiers envoyés depuis la machine dans un dossier que nous allons créer .Ce dossier sera le suivant : `public/uploads` à la racine du projet.

## Modification du service `event` 

Jusqu'à maintenant, dans tous les articles postés sur featherjs, nous n'avons jamais touché au code. Cette fois-ci nous allons légèrement y toucher.

## Envoi de fichier simple

A la création d'un événement (*dans notre service* `event`), nous avons la possibilité de joindre une **affiche** à cet événement.

Modifions le fichier `src/services/event/event.service.js`

Ajoutons

```javascript
const multer = require('multer');
const slugify = require('voca/slugify');

const storage = multer.diskStorage({
  destination: (_req, _file, cb) => cb(null, 'public/uploads'), // where the files are being stored
  filename: (_req, file, cb) => cb(null, `${Date.now()}-${file.originalname}`) // getting the file name
});
const upload = multer({
  storage,
  limits: {
    fieldSize: 100 // Max field value size `MB`
  }
});
```

Puis cette ligne

```javascript
app.use('/api/event', createService(options));
```

en

```javascript
app.use(
    '/api/event',
    upload.single('files'),
    (req, _res, next) => {
    const { method } = req;
    if (method === 'POST' || method === 'PATCH') {
      req.feathers.files = req.file; 
      const body = {
        name: req.body.name,
        flyer: req.file.filename,
        slug: slugify(req.body.name)
      };      
      req.body = body ;
    }
    next();
  }, createService(options));
```

- La fonction `upload.single('files')`indique l'envoi du fichier dont le nom est : `files`
- La variable `const body = {` contient des champs supplémentaires à envoyer au serveur avec le fichier.
- La ligne `slug: slugify(req.body.name)`fait appel à la fonction slugify de la librairie `voca/slugify`

Concrêtement, dans POSTMAN: 

- On choisit la methode: **POST**
- On saisit l'URL : **http://localhost:3030/api/event**
- Dans Body > form-data
  - On ajoute un champ "**files**" qui sera de type *file*
  - On ajoute un autre champ "**name**"

![](/images/featherjs/postman_upload.jpg)

Nous allons remplir ces champs :

- champ files > on choisit un fichier sur notre machine

- champ name > on saisit: "Maman j'ai raté l'avion"

- On s'assure que 

  - Dans "Authorization" > TYPE > "Bearer Token" > Champ Token à droite - le token est bien à jour. Sinon on s'authentifie comme on a fait dans l'{% post_link featherjs-authentication-upload-fr 'article précédent' %} et dans ce cas, on ajoute le token mis à jour dans ce champ.

  

  - On clique sur le bouton "SEND"

  Si l'enregistrement a bien été fait. On a le retour suivant:

  ![](/images/featherjs/postman_upload2.jpg)

  Et dans la base de données

  ![](/images/featherjs/mysql7.jpg)

  Remarquons le champ `slug` 

  La valeur "Maman j'ai raté l'avion" a été transformé en "maman-j-ai-rate-l-avion".


