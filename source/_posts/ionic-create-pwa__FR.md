---
title: Ionic - créer une PWA (Progressive Web Application) en quelques secondes
tags:
  - ionic
categories:
  - Tips-Tricks
permalink: ionic-create-pwa-fr
date: 2019-10-20 21:00:00
---

# PWA (Progressive Web Application)

Il s'agit d'une application web  qui agit comme une application mobile. C'est comme si on avait une application dans notre téléphone mais à partir d'une url dédiée. Elle est développée donc à partir des mêmes langages de programmations utilisés pour créer des sites internet : html, css et javascript.

# Ionic

Ionic est un framework qui permet de créer une PWA et une application hybride. Une application hybride est également une application dévelopée avec le(s) même(s) langage(s) de programmation (Javascript, css et html). Elle peut être lancée sur plusieurs supports différents (site internet à lancer dans un navigateur tel que google Chrome ou firefox et/ou en tant que application installée sur un smartphone) sans besoin d'adapter le code source. 

Habituellement, on utilise Ionic pour créer une application smartphone : Android, Iphone et windows Phone. Cependant, il peut être aussi utilisé pour créer une application web (PWA).

C'est ce que nous allons utiliser dans la suite de cet article.



# Ajouter @angular/pwa

Supposons que nous avons déja une application existante développée avec Ionic .

En ligne de commande, naviguons jusqu'au dossier de cette application. Ensuite ajoutons les librairies utiles:

```
ng add @angular/pwa
```

A la fin de l'installation, la commande nous retourne le résultat suivant dans le terminal.

![](/images/ionic-pwa/ng_angular_pwa.png)

Pour créer le code source dans le dossier **www** , Il faut ensuite lancer la commande suivante :

```
ionic build --prod
```

C'est le contenu de ce dossier (index.html et plusieurs javascripts) qu'il faudra déployer sur un serveur en  FTP par exemple .  

![Retour de la commande](/images/ionic-pwa/ng_angular_pwa4.png)

Voyons en détail les modifications apportées par ces 2 commandes :

## Modification du fichier angular.json

Ajout de 3 entrées :

- "src/manifest.json"
- "serviceWorker": true,
  "ngswConfigPath": "src/ngsw-config.json"

![](/images/ionic-pwa/ng_angular_pwa2.png)

et création de ces fichiers :

- src/manifest.json
- src/ngsw-config.json

# Packages supplémentaires 

2 packages supplémentaires ont été installés et le fichier package.json mis à jour en conséquence :

- @angular/pwa
- @angular/service-worker

## Modification de src/app/app.module.ts

Import de `@angular/service-worker`

```
import { ServiceWorkerModule } from '@angular/service-worker';
```

# Modification de src/index.html

Dans la balise `head`

```
  &lt;link rel="manifest" href="manifest.json"&gt;
  &lt;meta name="theme-color" content="#1976d2"&gt;
```

# FIN

Il ne nous reste plus qu'à envoyer l'ensemble des fichiers contenus dans le dossier www en FTP et apprécier le résultat à partir d'une url.  