---
title: Create a web and desktop application with Angular and electronJS
tags:
  - angular
  - electronjs
  - git
  - nodejs
categories:
  - [angular]
  - [electronjs]
permalink: en-angular-electronjs-get-started
date: 2019-06-19 00:40:07
---

![img](/images/angular-electronjs-get-started/12CGUAKR_kqP54k9IGO_5xA.png)

Prerequisite:

- nodejs
- git
- [@Angular/cli](http://twitter.com/Angular/cli)

# Installation of prerequisites

## Nodejs

Install nodejs. Go to https://nodejs.org/en/ and download the installer.

![img](/images/angular-electronjs-get-started/1zx4jcuwwpQEJVgGvFdWITA.png)

I took the version “12.4.0 Current”

We’re checking the intallation.

```
node -v
v12.4.0
```

## Git

Install git. Go to https://git-scm.com/

Check the correct installation

```
git version
git version 2.22.0.windows.1
```

## Angular/cli

Nodejs is required to install Angular/cli.

Install the latest version of Angular with npm (node package manager)

```
npm install @angular/cli
ng version
```



![img](/images/angular-electronjs-get-started/1vPWSyVHitV_z9_uBYOxILg.png)

### Angular-Electron

We’re going to make a copy (git clone) of Maxime GRIS’ excellent work.

```
git clone https://github.com/maximegris/angular-electron.git
```

We go into the folder of our application (in this case Angular-Electron) and install the dependencies

```
cd angular-electron
npm install
```

Launch the default web application in a browser with the following command

```
npm run ng:serve:web
```

The url should be available at http://localhost:4200/#/ . 

To start the desktop software in development mode. Type the following command:

```
npm start
```

A window starts and you get this

![img](/images/angular-electronjs-get-started/1M6oCEtJykenXEIItvQhJmQ.png)

```
npm run build:prod
```

This command will compile the web application in mode — **aot** . I refer you to the excellent article by [Sébastien Olivier](https://sebastienollivier.fr/blog/angular/angular-jit-vs-aot) which explains the usefulness of this method of compilation **aot (ahead of time)**

Once the application is compiled, simply move all the files in the dist/ directory to an FTP directory for example.

Creating an Executable application when it is very easy also. It can take some time. This is normal. It is done with the following command:

```
npm run electron:windows
```

In the release/folder

![img](/images/angular-electronjs-get-started/1VdSGWTRQuApaNYs-0dgqmA.png)

Sources: https://github.com/rabehasy/angular-electron/tree/start

Thanks to A.A for translating this article

Published on **[Medium](https://medium.com/@miarirabs/create-a-web-and-desktop-application-with-angular-and-electronjs-79c93ce1e21d)**