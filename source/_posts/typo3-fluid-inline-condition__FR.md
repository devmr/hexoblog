---
title: TYPO3 Fluid Template Condition dans un attribut html
tags:
  - fluid
  - typo3
  - cms
categories:
  - - Tips-Tricks
  - - TYPO3
permalink: typo3-fluid-inline-condition-fr
date: 2019-08-20 20:40:07
---




....

# Contexte et mise en place.

On veut afficher des "*actualités (news)*" issues d'une base de données en l'occurence, la table **tx_news_domain_model_news**.

Ces actualités sont affichées grâce à une balise `li` . La contrainte qu'on s'imposera : Il ne faut afficher que les 4 premiers menus. Tous les autres doivent être masquées. Ces dernières seront masquées grâce à l'attribut bien connu `style="display: none;"` qu'on attachera à la balise `li`.

Voici le rendu html final souhaité.

{% googleads 'in-article' 'fluid' '8275635211' %}

{% gist fc62142666bc15586bb0e7ca7488cbd2 render.html %}

# Mise en place avec fluid template

**Fluid** est le moteur de template utilisé généralement par TYPO3. Il est couplé avec **extbase** lors de la création d'une extension.

Voici le bout de code fluid pour afficher ces news.

{% gist 5424f02a83552e5a92f6d5c1111cb03b fluid_typo3_inline_condition.html %}