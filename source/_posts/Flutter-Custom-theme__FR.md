---
title: Thème/Couleurs personnalisés dans Flutter
tags:
  - dart
  - flutter
categories:
  - [flutter]
permalink: flutter-custom-theme
date: 2019-06-28 22:40:07
---


Une application Flutter est créée par défaut avec la couleur bleue comme sur cette capture.

![](/images/Flutter-—-creer-une-application-sous-Android…_images/c5401213.png)

Cet article décrit les étapes à suivre pour personnaliser les couleurs de l'application. Par exemple, nous voulons que cette application respecte les codes couleurs définis par le service marketing de l'entreprise. 

Voici les étapes à suivre  :

- Trouver la bonne combinaison de couleur.
- Récupérer les codes hexadécimaux de ces couleurs
- Intégrer ces couleurs dans le thème.
- Tester le rendu dans l'application.

# Trouver la bonne combinaison de couleur.

Allons sur [ce lien](https://material.io/tools/color/#!/?view.left=0&view.right=0) pour créer cette combinaison de couleur personnalisée. Sur cette page, on peut choisir parmi les couleurs prédéfinies ou en créer une nouvelle. Ce choix se fait à partir des 2 onglets "MATERIAL PALETTE" et "CUSTOM"

![](/images/flutter-custom-theme/sc_tab.jpg)

Pour ma part, j'ai fait au plus simple. J'ai choisi la couleur suivante "**#b71c1c**" grâce au premier onglet "MATERIAL PALETTE".

# Codes hexadécimaux des couleurs

Une fois la couleur choisie, on exporte le fichier color.xml - Export > Android 

![](/images/flutter-custom-theme/export.jpg)

![](/images/flutter-custom-theme/export2.jpg)

Ce fichier color.xml doit ressembler à ceci

```xml
<!--?xml version="1.0" encoding="UTF-8"?-->
<resources>
  <color name="primaryColor">#b71c1c</color>
  <color name="primaryLightColor">#f05545</color>
  <color name="primaryDarkColor">#7f0000</color>
  <color name="primaryTextColor">#ffffff</color>
</resources>
```

# Intégration de ces couleurs dans le thème 

Créons le fichier lib/themes/color.dart

```dart
import 'package:flutter/material.dart';


const PrimaryColor = const Color(0xFFb71c1c);
const PrimaryColorLight = const Color(0xFFf05545);
const PrimaryColorDark = const Color(0xFF7f0000);

const SecondaryColor = const Color(0xFFb2dfdb);
const SecondaryColorLight = const Color(0xFFe5ffff);
const SecondaryColorDark = const Color(0xFF82ada9);

const Background = const Color(0xFFfffdf7);
const TextColor = const Color(0xFFffffff);

class MyTheme {
  static final ThemeData defaultTheme = _buildMyTheme();

  static ThemeData _buildMyTheme() {
    final ThemeData base = ThemeData.light();

    return base.copyWith(
      accentColor: SecondaryColor,
      accentColorBrightness: Brightness.dark,

      primaryColor: PrimaryColor,
      primaryColorDark: PrimaryColorDark,
      primaryColorLight: PrimaryColorLight,
      primaryColorBrightness: Brightness.dark,

      buttonTheme: base.buttonTheme.copyWith(
        buttonColor: SecondaryColor,
        textTheme: ButtonTextTheme.primary,
      ),

      scaffoldBackgroundColor: Background,
      cardColor: Background,
      textSelectionColor: PrimaryColorLight,
      backgroundColor: Background,

      textTheme: base.textTheme.copyWith(
          title: base.textTheme.title.copyWith(color: TextColor),
          body1: base.textTheme.body1.copyWith(color: TextColor),
          body2: base.textTheme.body2.copyWith(color: TextColor)
      ),
    );
  }
}
```

Nous définissons nos couleurs telles que définies dans le fichier color.xml comme ceci dans le fichier lib/themes/color.dart

```
const PrimaryColor = const Color(0xFFb71c1c);
const PrimaryColorLight = const Color(0xFFf05545);
const PrimaryColorDark = const Color(0xFF7f0000);

const SecondaryColor = const Color(0xFFb2dfdb);
const SecondaryColorLight = const Color(0xFFe5ffff);
const SecondaryColorDark = const Color(0xFF82ada9);

const Background = const Color(0xFFfffdf7);
const TextColor = const Color(0xFFffffff);
```

Il s'agit juste de préfixer le code hexadécimal par  "0xFF" :

- **b71c1c** --> 0xFF**b71c1c**

Dans lib/main.dart, on ajoute l'import du fichier du theme.

```dart
import './themes/color.dart';
```

Dans la déclaration du thème, à la place de 

```dart
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Flutter application MR'),
    );
```

on remplace par ceci 

```dart
    return MaterialApp(
      title: 'Flutter Demo',
      theme: MyTheme.defaultTheme,
      home: MyHomePage(title: 'Flutter application MR'),
    );
```

A la ligne 4 du code précédent, nous avons juste appeler la propriété static **defaultTheme** de la classe **MyTheme** qui est du type **ThemeData**

Par la même occasion, on supprime la barre debug. Le code ci-dessus devient.

```dart
    return MaterialApp(
      title: 'Flutter Demo',
      theme: MyTheme.defaultTheme,
      debugShowCheckedModeBanner: false,  
      home: MyHomePage(title: 'Flutter application MR'),
    );
```



# Tester le rendu

![](/images/flutter-custom-theme/Screenshot_1.jpg)

## Webographie et Inspirations

- https://blogs.infinitesquare.com/posts/mobile/flutter-ajouter-un-theme-a-votre-application



