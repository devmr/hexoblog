---
title: Flutter — créer une application sous IOS
tags:
  - flutter
  - ios
  - dart
categories:
  - [flutter]
permalink: flutter-fr-ios-get-started
date: 2019-06-11 00:40:07
---

![img](/images/ios/0PfXjCEM_KyG6phyU.png)

Après une première introduction sur la création d’une première application avec de cet article {% post_link flutter-android-get-started 'Android' %} , on fait la même manipulation et cette fois ci avec IOS.

La création d’une application avec IOS en soit est facile. Il n’y a pas de différence avec Android. Ah mais oui, on est sur Flutter.

Cet article va donc être court et se focalisera plutôt sur mon vécu de cette expérience. Pour résumer, l’étape la moins évidente était le déploiement de l’application sur un “**real device**”, en l’occurence l‘iPhone.

## Prérequis au deploiement sur un device

### xcode

xcode est le logiciel incontournable si on veut faire fonctionner une application sur un iphone ou un ipad.

On va sur https://developer.apple.com/develop/ et on télécharge xcode. On en profite également pour créer un compte developer Apple si on n’en n’a pas déja un. L’inscription est gratuite. Avoir un compte developer est obligatoire pour pouvoir tester une application sur un iPhone.

### Homebrew

[Homebrew](https://brew.sh/index_fr) est le gestionnaire de package de macOS au même titre que [chocolatey](https://chocolatey.org/) pour windows ou [aptitude](https://doc.ubuntu-fr.org/aptitude) pour linux. Il nous sera utile pour installer quelques librairies ayant pour objectif de faire communiquer notre application à IOS.

On ouvre un terminal et on tape la commande suivante pour installer homebrew

```shell
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Une fois homebrew installé, on va pouvoir installer les différents packages utiles.

Allons-y pour installer tout ça.

```shell
brew update 
brew install — HEAD usbmuxd
brew link usbmuxd
brew install — HEAD libimobiledevice
brew install ideviceinstaller
brew install cocoapods
```

usbmuxd , libimobiledevice et ideviceinstaller sont des librairies permettant de communiquer avec IOS nativement >> http://www.libimobiledevice.org/

[Cocoapods ](https://cocoapods.org/)quand à lui est un gestionnaire de package pour Swift et Objective-C. *Rassurons-nous, on ne va pas apprendre ces 2 langages. Pour rappel, ce sont les langages de programmation utilisés souvent pour créer une application IOS*.

On paramètre pod

```
pod setup
```

## Installons Flutter

On va donc télécharger le SDK pour mac ici https://flutter.dev/docs/get-started/install/macos

![img](/images/ios/11KKismcmOcQRDBpWs1Sykw.png)

On crée le dossier /Users/mr/flutter et on extrait le zip téléchargé dans ce dossier.

On crée/ajoute une entrée dans notre variable d’environnement PATH qui aura la valeur suivante : **/Users/mr/flutter/bin**

Dans le terminal, on tape ceci

```
export PATH="$PATH:/Users/mr/flutter/bin"
```

On test le bon fonctionnement en tapant

```
echo $PATH
```

# Check de flutter avant la création de l’application

```
flutter doctor
```

On doit avoir les informations suivantes en retour

```
[✓] Flutter (Channel stable, v1.5.4-hotfix.2, on Mac OS X 10.13.6 17G7024,
 locale fr-FR)
[✗] Android toolchain — develop for Android devices
 ✗ Unable to locate Android SDK.
 Install Android Studio from:
 https://developer.android.com/studio/index.html
 On first launch it will assist you in installing the Android SDK
 components.
 (or visit https://flutter.dev/setup/#android-setup for detailed
 instructions).
 If the Android SDK has been installed to a custom location, set
 ANDROID_HOME to that location.
 You may also want to add it to your PATH environment variable.
[✓] iOS toolchain — develop for iOS devices (Xcode 10.1)
[!] Android Studio (not installed)
[✓] Connected device (1 available)
! Doctor found issues in 2 categories
```

Jusqu’ici, on est bon car on n’a pas besoin de Android.

## Créons notre 1ere app

Dans le terminal, on va dans un dossier par exemple /Users/mr/flutterapps et on tape ceci :

```
flutter create firstapp
```

Que fait cette commande :

- Elle crée le dossier **firstapp**
- Dans le dossier **firstapp**, elle crée une application qui utilisera le Material Components.

## Lançons cette app…sur notre iPhone

Je vais d’abord citer les versions des outils utilisés dans cette démo :

- xcode: 10.1
- IOS: 12.3.1

On lance xcode et on ouvre le fichier **Runner.xcodeproj** à la racine du projet**.**

Ensuite dans le volet de gauche de xcode, on clique sur Runner

![img](/images/ios/1-8yZF736iYK9nVznvRVBEA.png)

Dans la colonne centrale puis dans le champ **Status**, une icône rouge nous informe qu’il manque quelque chose

![img](/images/ios/1zDqIlP9WF1ZpJgd_80_V4w.png)

C’est dans ce champ qu’on ajoute notre compte developer ou on ajoute un compte. On choisit notre nom dans le champ **Team** puisque je l’ai déjà ajouté.

![img](/images/ios/1QJvifvJ5DIvHNbhTahZDrw.png)

On aura donc la configuration suivante et 2 autres icônes rouges apparaissent.

![img](/images/ios/1dUJwJ8DvljCBqXQjORxCdg.png)

Car l’identifiant champ “Bundle Identifier” **com.example.firstapp** est la valeur par défaut et cet identifiant a déjà été utilisé par une autre application dans Apple Store (surement la 1ère application flutter ;). On va juste changer la valeur du champ “Bundle Identifier” en **com.flutter.miary** comme sur la capture et le problème est reglé.

![img](/images/ios/1U6DpkOQUx5SJSBq1KPIYHw.png)

On est bon coté xcode. On va dans les paramètres de l’iPhone pour lui préparer à accepter l’application une fois le bouton “lancement” cliqué à partir de xcode.

On va dans **Reglages**

![img](/images/ios/1be1KBSOhcQ5UzSpaiVWM3w.png)

… puis sur **Gestion de l’appareil**

![img](/images/ios/1JOgM3U8w4r6-GZnACWAasw.png)

…on clique sur notre nom

![img](/images/ios/1CMLTc96bLsnUF1hRdvTY9w.png)

On retourne dans xcode. On va lancer maintenant l’application à partir de xcode vers notre telephone. On choisit notre telephone dans la liste des devices proposés dans xcode

![img](/images/ios/19yBq_NngqNQMLgO6VNTmbQ.png)

…puis sur l’icone “Build”

![img](/images/ios/1YGTl6fV8F7f_c456ppywJw.png)

Dans notre iPhone , on autorise l’installation de l’application en cliquant sur “Faire confiance à moncompte@dev”

![img](/images/ios/1ZiITg7gApXeAXPctgRZa2A.png)

On clique sur “OUI” dans la popup

![img](/images/ios/10HpUC6eMePQtbuEL8DYv_Q.png)

Et là, on a notre application lancé dans notre telephone

![img](/images/ios/1A1vIXnwDaOXybtCXOHFOVQ.png)

Cet article est également publié sur **[Medium](https://medium.com/@miarirabs/flutter-1%C3%A8re-application-avec-ios-c67b977fba92)**