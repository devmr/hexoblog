---
title: Flutter — créer une application sous Android…
date: 2019-06-10 23:59:00
tags:
- flutter
- dart
- android
categories:
- [flutter]
permalink: flutter-android-debuter
---
![](/images/Flutter-—-creer-une-application-sous-Android…_images/53f6ff6b.png)

Pourquoi Flutter a suscité mon intérêt ? D’une part, parce que… je ne m’emballais pas trop avec les outils ReactNative/NativeScript ni les framework hybrides tels que Cordova/Xamarin… (*quoique j’ai un faible pour Ionic hic* ;). Aussi, je connais le développement Android JAVA mais pas Swift. *Au passage, j’ai créé une application full Android avant la montée en popularité de Kotlin* :)

[Flutter](https://flutter.dev/) est un framework Open source créé par Google. Il permet de créer une application mobile native sous Android et IOS.

## …Origines

![](/images/Flutter-—-creer-une-application-sous-Android…_images/1fa1f87c.png)

*Flutter a été créé avec le langage [Dart](https://dart.dev/) (lui même créé par Google). Dart avait été créé dans l’objectif de concurrencer JavaScript, de créer des applications plus rapides et d’ajouter des fonctionnalités manquantes au JavaScript telles que les classes par exemple…*

Pour en revenir à nos moutons, je veux créer une application mobile native compatible avec Android et IOS sans devoir **apprendre** à/et **coder** avec 2 langages différents (Android/kotlin et swift). Le choix de Flutter m’a donc paru évident ;) Au final, je ne me concentre que sur mon code dart. Le reste du travail est confié à Android pour faire en sorte que mon appli fonctionne.

Flutter est très bien documenté. Une application basique Flutter se lance en (à peine) 5 minutes.

## Installons Flutter

Je bosse généralement avec Windows. J’utilise un simple éditeur de texte tel que Sublime text et la ligne de commande pour développer avec Flutter : création d’une application, lancement dans un simulateur ou un device, mise à jour d’un package…

On va sur https://flutter.dev/docs/get-started/install/windows et on télécharge le zip

![](/images/Flutter-—-creer-une-application-sous-Android…_images/77ee6eec.jpeg)

On extrait le zip téléchargée dans **C:\flutter**

On crée/ajoute une entrée dans notre variable d’environnement PATH qui aura la valeur suivante : **C:\flutter\bin**

On a le choix de faire cela manuellement ou en ligne de commande. Si on choisit de faire cette manip. en ligne de commande, on ouvre une **console DOS** et on tape la commande suivante :

```
PATH %PATH%;C:\flutter\bin
```

Pour vérifier, on saisit

```
path
```

**C:\flutter\bin** doit être ajoutée à la liste. On n’hésite pas à redémarrer la console DOS si besoin (et s’il arrive que cela ne fonctionne pas du premier coup)

### Check de flutter avant la création de l’application

```
flutter doctor
```
Il se peut qu’on ait les lignes suivantes en retour

```
[√] Flutter (Channel stable, v1.5.4-hotfix.2, on Microsoft Windows [version 10.0.17134.765], locale fr-FR)
[!] Android toolchain — develop for Android devices (Android SDK version 28.0.3)
 ! Some Android licenses not accepted. To resolve this, run: flutter doctor — android-licenses
[!] Android Studio (not installed)
```

Dans ce cas, on tape ceci pour ajouter les licences Android.

```
flutter doctor --android-licenses
```

A cette étape, si Android Studio n’est pas installé sur la machine, cela fonctionnera quand même. Ce n’est pas une information bloquante donc.

## Créons notre 1ere app

Il est temps de passer à l’acte. On ouvre une console DOS dans un dossier sur notre disque dur, et on tape ceci :

```
flutter create monAppli
```

Que fait cette commande :

- Elle crée le dossier monAppli
- Dans ce dossier, elle crée une application qui utilisera le Material Components.

*PS: Petite blague, j'ai voulu faire*

```
flutter create flutter
```

*Pourquoi? par flemme - mais aussi par pur hasard ! et bien, je me suis retrouvé avec l'erreur suivante*

`Invalid project name: 'flutter' - this will conflict with Flutter package dependencies.`

*Bon à savoir donc*

## Lançons cette app

Toujours dans la console DOS, on va dans le dossier monAppli et on lance run

```
cd monAppli
flutter run
```

Observons la magie dans notre simulateur ou notre téléphone directement tant qu’on y est ;)

Et voilà… on a créé notre première appli.

![](/images/Flutter-—-creer-une-application-sous-Android…_images/c36f916f.png)

## Modifions un peu cette app

On va dans notre éditeur de texte , et on ouvre le fichier lib\main.dart.

On va modifier le titre . On remplace

```
home: MyHomePage(title: ‘Flutter Demo Home Page’),
```

par 

```
home: MyHomePage(title: ‘Flutter application MR’),
```

Puis le texte

```
Text(
 ‘You have pushed the button this many times:’,
 ),
```

par 

```
Text(
 ‘Nb de Clic effectué sur ce bouton :’,
 ),
```

Dans notre console DOS, on appuie sur la touche “**r**” de notre clavier. On regarde le résultat dans l’application

![](/images/Flutter-—-creer-une-application-sous-Android…_images/c5401213.png)

## Et si…

Parce que la vie nous réserve dès fois quelques surprises. Voici des “Et si…”

### J’ai plusieurs simulateurs.

Et que je veux lancer l’appli sur tous les simulateurs y compris mon téléphone.

Ok — au lieu de faire flutter run, on fait

```
flutter run -d all
```

Ou sur un seul simulateur dans la liste

```
flutter run -d <deviceid>
```

Pour avoir le “deviceid” on fait

```
flutter devices
```

On a un retour comme ceci

```
1 connected device:
Nokia 2 • E1MGAP27C2249175 • android-arm • Android 7.1.1 (API 25)
```

Il suffit ensuite de faire

```
flutter run -d E1MGAP27C2249175
```

pour lancer l’application sur ce device uniquement.

### Je ne veux pas installer Android Studio

*tout en ayant quand même le SDK android qui tourne*

Et ben, on passe par [Chocolatey](https://chocolatey.org/) (un gestionnaire de package sur windows)

On lance un powershell pour installer chocolatey. Et on tape:

```
@powershell -NoProfile -ExecutionPolicy unrestricted -Command “iex ((new-object net.webclient).DownloadString(‘https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin
```

Lorsque Choco est installé, on va pouvoir installer **JDK** et **android-sdk** avec.

```
choco install adoptopenjdk — version 8.192
choco install android-sdk gradle -y
```

On installe ensuite le SDK manager (permet d’installer une version quelconque de Android)

```
"%ANDROID_HOME%\tools\bin\sdkmanager" "emulator" "platform-tools" "platforms;android-28" "build-tools;28.0.3" "extras;android;m2repository" "extras;google;m2repository"
```

## Autour de flutter

Exemples d’applications développés avec flutter : https://itsallwidgets.com/

Les packages: https://pub.dev/flutter

{% post_link flutter-android-get-started 'Version anglaise' %} de cet article

{% blockquote Medium https://medium.com/@miarirabs/flutter-dev-1ere-exp%C3%A9rience-ca02e5aeaeec %}
Cet article est également disponible sur Medium
{% endblockquote %}

## Mise à jour du 05 nov 2019

### Upgrade de flutter

C'est très simple

```
flutter upgrade
```

Et le tour est joué.






