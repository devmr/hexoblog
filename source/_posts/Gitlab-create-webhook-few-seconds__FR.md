---
title: Gitlab - mettre en place un webhook en quelques secondes
tags:
  - webhook
  - gitlab
categories:
  - - Tips-Tricks
permalink: fr-gitlab-create-webhook-few-seconds
date: 2019-08-14 00:40:00
---


Un **webhook** est une méthode permettant d'ajouter ou de modifier le comportement d'une page grâce à un retour d'appel (*callback*). Le format est le **JSON** et la requête utilisée est la méthode **POST**.

Dans le cas de [gitlab](gitlab.com) ou de github, un webhook est utilisé par exemple pour déclencher un événement suite à un nouveau **push** dans un repository ou à la création d'une **issue** (*report d'une anomalie*). Cet événement lance une URL ajoutée dans gitlab en utilisant la méthode POST.

{% googleads 'in-article' 'fluid' '8275635211' %}

Pré-requis pour la mise en place d'un webhook dans gitlab :

- Créer ou choisir un repository dans gitlab
- Créer ou utiliser une url qui accepte la méthode POST 

# Repository dans Gitlab

Nous allons partir d'un repository déja créé dans Gitlab. 

- Dans projet > Settings > Intégrations > 

- Dans le champ URL: saisissons l'URL du webhook: exemple - https://monsite.com/githooks.php 

- Cochons les cases suivantes :

  -  "**Push events**"
  - "**Merge request events**"
  - **Enable SSL verification**

- Et cliquons sur "**SAVE**"

  ![urlfield](/images/webhook/urlfield.jpg)

# Code du script githooks.php

L'objectif du script githooks.php est de faire une mise à jour du dossier en faisant un `pull`du repository gitlab.

Ce script peut être appelée de 3 façons: 

- Via gitlab en http suite à un push
- Via GET par un utilisateur qui connait l'URL
- Via la ligne de commande qui éxecute la commande `git pull`

Voici le code complet du script

{% gist 04710fc30cc058e62748302aa6c26f9c  githooks.php %}

## Appel de gitlab en http suite à un push

Il s'agit des lignes : L23 - L47. Le script ne fait pas grand chose de très intéressant à part :

- Enregistrer la requete dans un dossier temporaire et dans un fichier texte.
- Envoie un mail à mon adresse.

## GET par un utilisateur qui connait l'URL

Il s'agit de la ligne L 50 . Cette ligne affiche un texte 

![urlfield](/images/webhook/hello.jpg)

## ligne de commande qui éxecute la commande `git pull`

Dans un premier temps, je me connecte en SSH sur le serveur. Je crée ensuite un cron qui appelle ce script toutes les minutes en utilisant l'utilitaire `php`. Voici la ligne du crontab

```
* * * * * php /home/web/githooks.php
```

Lignes du script:  L11 - 21

Si le fichier texte existe dans le dossier temporaire lorsque gitlab a appelé le hook, alors la commande suivante est lancée :

```
cd /home/web/miaryrabs/
git pull gitlab master
rm -rf /tmp/*
```

- Je vais dans le dossier /home/web/miaryrabs/ qui est le dossier de mon site par exemple
- Je lance la commande git pull gitlab master car je veux mettre à jour les dossiers/fichiers de mon site à partir des fichiers que je viens de "pusher" dans mon repository.
- Je supprime le contenu de mon dossier temporaire pour éviter les boucles infinies au prochain lancement du crontab.

{% googleads 'in-article' 'fluid' '8275635211' %}

# Conclusion

On a cécrit les manipulations à faire dans le cadre d'une création d'un webhook pour un repository gitlab. Il en est de même pour un projet github. Le principe reste le même. Les champs peuvent différer. Cela sera l'objet d'un autre article.

A bientôt ;)



