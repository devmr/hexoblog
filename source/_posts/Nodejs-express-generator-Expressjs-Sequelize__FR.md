---
title: expressjs —Connexion à une base de données avec sequelize et ses extensions (sequelize-cli, sequelize-auto…)
tags:
  - nodejs
  - expressjs
  - sequelize
categories:
  - [nodejs]
  - [expressjs]
  - [sequelize]
permalink: expressjs-connexion-à-une-base-de-donnees-avec-sequelize-et-ses-extensions-sequelize-cli
date: 2019-06-17 00:40:07
---

![img](/images/express-sequelize/0rwd6KeolcXgz7zpx.png)

Cet article est la suite d’{% post_link create-a-nodejs-express-application-with-express-generator 'un premier article' %} sur la création d’une application web en nodejs grâce au package express-generator. Cette fois ci, notre objectif est d’ajouter une connexion MySQL pour pouvoir récupérer les informations depuis une base de données et afficher ces informations dans l’application sous forme de liste simple.

On utilisera MySQL comme base de données . Pour cela, nous allons nous servir d’un [ORM (Object Relational Mapping)](https://fr.wikipedia.org/wiki/Mapping_objet-relationnel). Il existe plusieurs ORM nodejs tels que : [sequelize](http://docs.sequelizejs.com/), [typeORM](https://typeorm.io/#/) et [mongoose](https://mongoosejs.com/). L’ORM le plus adapté dans notre cas est **sequelize**. Sequelize permet l’utilisation d’une base de données de type : MySQL, Postgres, SQLite et Microsoft SQL Server.

On ajoutera également d‘autres packages nodejs utiles telles que : sequelize-auto, sequelize-cli (qui sont des [extensions de sequelize](http://docs.sequelizejs.com/manual/resources.html#addons--amp--plugins)) , mysql2 et mysql.

- mysql et mysql2 sont 2 librairies obligatoires pour être utilisé avec sequelize et sequelize-auto. sequelize a besoin de **mysql2** pour fonctionner correctement et **sequelize-auto** a lui besoin de mysql .

On installe ces packages en tapant ceci dans une console.

```
npm install sequelize sequelize-auto sequelize-cli mysql mysql2 --save
```

Comme sequelize-cli et sequelize-auto seront installées dans le même dossier de l’application, on utilisera **npx** : ( un utilitaire qui permet d’exécuter des binaires à partir de paquets npm).

Exécutons

```
npx sequelize-cli init
```

Cette commande crée 4 dossiers:

- models
- le fichier models/index.js
- config
- le fichier config/config.json
- migrations
- seeders

On modifie le fichier config/config.json et on remplit par les informations de notre BDD

```
{
  "development": {
    "username": "root",
    "password": "*****",
    "database": "dbname",
    "host": "localhost",
    "dialect": "mysql",
    "operatorsAliases": false
  },
  "test": {
    "username": "root",
    "password": "*****",
    "database": "dbname_dev",
    "host": "localhost",
    "dialect": "mysql",
    "operatorsAliases": false
  },
  "production": {
    "username": "root",
    "password": "*****",
    "database": "dbname_prod",
    "host": "localhost",
    "dialect": "mysql",
    "operatorsAliases": false
  }
}
```

Dans notre exemple, sequelize utilisera les informations de connexion contenues dans “developpement”. On peut savoir cela grâce à la ligne suivante dans le fichier models/index.js

```
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
```

Utilisons la commande suivante pour créer un nouveau model.

```
npx sequelize-cli model:generate - name User - attributes firstName:string,lastName:string,email:string
```

Comme précisé plus haut, grâce au package sequelize-auto, on pourra générer automatiquement nos models à partir de tables existantes, ce qui est notre cas, car on a déjà une base de données prête.

```
npx sequelize-auto -o "./models" -d dbname -h localhost -u root -p 3306 -x motdepasse -e mysql
```

## Récupération des données

On modifie le fichier bin/www . On ajoute

```
var models = require('../models');
```

Puis

```
models.sequelize.sync().then(function() {
    /**
     * Listen on provided port, on all network interfaces.
     */
    server.listen(port, function() {
        debug('Express server listening on port ' + server.address().port);
    });
    server.on('error', onError);
    server.on('listening', onListening);
});
```

pour synchroniser automatiquement les models à la base de données.

### Afficher les infos dans routes/users.js

On va modifier le fichier routes/users.js et ajouter

```
const db = require('../models');
```

Puis dans le bloc

```
router.get('/', function(req, res, next) {
    db.api.findAll({ limit: 10 }).then(function(rows) {
       res.render('user', { rows: rows });
   });
});
```

Dans l’exemple ci-dessus — **api** est le nom du model models/api.js qui ressemble à ceci	

```
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('api', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'api',
    underscored: true
  });
};
```

Voici un aperçu de la table api

![img](/images/express-sequelize/1WaSDIeoqtePoUp2MY4MOOA.png)

# Template handlebars

Aussi modifions views/user.hbs comme ceci

```
{{#each rows}}
    <div class="border-bottom__1px_solid_black">
        <h2>{{name}}</h2>
        {{{created_at}}}
    </div>
{{/each}}
```

qui affiche simplement une boucle “**#each” : name** et **created_at** sont des champs dans la table **api**.

On va ensuite sur l’url http://localhost:3000/users et on devrait avoir ceci comme résultats

![img](/images/express-sequelize/1Rs2HzG2cSCX7GxjC7hna7Q.png)

Sources: https://github.com/rabehasy/nodejs-express/tree/step1

Ce post est également publié sur **[Medium](https://medium.com/@miarirabs/expressjs-connexion-%C3%A0-une-base-de-donn%C3%A9es-avec-sequelize-et-ses-extensions-sequelize-cli-7c6118d12c01)**