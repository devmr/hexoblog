---
title: Android Studio - Select Android SDK Home
tags:
  - android
categories:
  - - Tips-Tricks
permalink: android-error-select-android-sdk-home-fr
date: 2019-09-21 21:00:00
---

# Contexte



Android Studio n'arrive pas à trouver le SDK android bien que ce dernier soit bien téléchargé et décompréssé dans le dossier **C:\Android\android-sdk**

Je me retrouve avec l'erreur suivante :

![](/images/android/Select-android-SDK-Home.png)

# Solution

Dans Android Studio > File > Settings > Appearance & Behavior > System settings > Android SDK 

On clique sur **Edit**

![](/images/android/Select-android-edit.png)

Dans la fenêtre, on clique sur "**Next**" Jusqu'à la fin 

Je vide les caches et je redémarre en allant dans `File &gt; invalid cache and restart`



