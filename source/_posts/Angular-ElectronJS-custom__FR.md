---
title: Angular-Electronjs - Icônes, nom personnalisé - Installateur avec NSIS
tags:
  - electronjs
  - nsis
  - electron-builder
categories:
  - [electronjs]
  - [angular]
permalink: angular-electronjs-custom-config-electronjs
date: 2019-07-06 00:40:07
---


Dans l'article « {% post_link angular-electronjs-storage-service 'Angular-Electronjs - Enregistrement du token grâce à l\'API fs de Nodejs' %} », nous avons fait confiance à **[electron-builder](https://www.electron.build/)** (*le compilateur de electronjs*) et donc avons laissé les paramètres par défaut lors de la création de l'application **electronjs**

Ces paramètres par défaut sont :

- le nom de l'éxécutable généré : "**angular-electron Setup 6.0.1.exe**"
- Le nom de la fenêtre : "**AngularElectron**"
- L'icone de l'application 
- La barre de menu  

1. Nous allons donc changer ces informations.
2. Nous allons modifier le texte qui apparaît au chargement de l'application.

# Nom de l'éxécutable 

Par défaut, le nom de l'éxécutable créé est: **angular-electron 6.0.1.exe**.

![](/images/angular-electronjs/app.jpg)

Nous allons changer ce nom  en "**Madatsara Setup 6.0.1.exe**" . Aussi,  nous ajouterons l'outil **NSIS** . En effet NSIS permet de créer un installateur/desinstallateur pour notre application.

Modifions le fichier angular-electron/electron-builder.json. On remplace `productName`par

```h
"productName": "Madatsara",
```

... On ajoute "NSIS" dans 

```json
"win": {
    "icon": "dist",
    "target": [
      "NSIS"
    ]
  }
```



# Nom de la fenêtre

Le nom par défaut de la fenêtre est   : "**AngularElectron**"

![](/images/angular-electronjs/window.jpg)

Nous allons changer ce nom en "**Madatsara**". Pour cela, modifions le fichier : angular-electron/src/index.html et changeons la balise `title` par

{% gist 2f2f40bc92a03d18472a1bdf3417f60d title.html %}

# Icône par défaut

Voici l'icône par défaut de l'application  .

![](/images/angular-electronjs/icon.jpg)

Nous allons remplacer cette icône par celle ci.

![](/images/angular-electronjs/newicon.jpg) 

Pour cela, on remplace toutes les icônes par défaut .ico et .png dans le dossier /src

# Barre de menu

![](/images/angular-electronjs/menu.jpg)

Nous allons supprimer ce menu. 

Modifions le fichier angular-electron/main.ts

Après

```
win = new BrowserWindow({
    x: 0,
    y: 0,
    width: size.width,
    height: size.height,
    webPreferences: {
      nodeIntegration: true,
    },
  });
```

On ajoute 

```
// remove menu
  win.setMenu(null);
```

# Changement de l'apparence du texte "Loading..."

Il est possible de rendre l'apparence du texte "loading..." qui apparaît avant le chargement de l'application. 

Notre objectif:

Remplacer ce texte

![](/images/angular-electronjs/oldloading.jpg)

Par celui-ci :  couleur de fond , icône animée, texte centré et taille de la police augmentée.

![](/images/angular-electronjs/newloading.jpg)

Ce texte se situe dans le fichier angular-electron/src/index.html. C'est donc ce fichier qu'il convient de modifier.

Modifions

{% gist c37654bb7a6e3b454e8a8296552a796e app_root.html %}

Par

{% gist 1d59322052001d66bb09499ec4ee24ec app_root_custom.html %}



# Génération de l'application

Une fois ces modifications faites.. On supprime les contenus des dossiers `release`et  `dist`. Ensuite, on crée l'éxécutable de l'application grâce à la commande suivante.

```
npm run electron:windows
```

Une fois la commande terminée, on retrouve les éléments suivants dans le dossier   `\angular-electron\release`

![](/images/angular-electronjs/newrelease.jpg)

On double clique l'installateur madatsara-app 1.0.0 Setup 6.0.1.exe

L'application sera alors installée dans le dossier `C:\Users\~\AppData\Local\Programs\angular-electron`

Et on distingue bien un autre  executable qui permet  de désinstaller l'application

![](/images/angular-electronjs/programm.jpg)

# En résumé

Missions accomplies :

* Le nom de la fenêtre a bien changé. ✔️

![](/images/angular-electronjs/newwindow.jpg) 

- Le menu a disparu ✔️
- Les icônes remplacées. ✔️

