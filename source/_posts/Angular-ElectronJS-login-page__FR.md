---
title: Angular-ElectronJS — Page de connexion
tags:
  - angular
  - electronjs
  - fontawesome
categories:
  - [angular]
  - [electronjs]
permalink: angular-electronjs-page-de-connexion
date: 2019-06-21 00:40:07
---



On entre dans le vif du sujet de notre application Angular/electronjs. Créons une page de connexion dans notre application **web**.

Cette page de connexion communiquera plus tard avec un backend nodejs développée grâce à expressjs. Cette application est déja fonctionnelle. Elle utilise l’ORM sequelize pour se connecter à une base de données MySQL.

Voici un {% post_link en-expressjs-api-rest-with-sequelize 'article' %} détaillant la mise en place de cet application.

Pour le design général de l’interface, on aurait pu choisir [bootstrap ](https://getbootstrap.com/)tout simplement . Nous allons faire mieux et surtout suivre la norme angular ;) On utilisera alors [Material Angular](https://material.angular.io/).

Plusieurs raisons nous amène à ce choix:

- Parce que Material Angular est développé par Angular donc s’intègre facilement à une application.
- Plusieurs [composants](https://material.angular.io/components/categories) existent et sont toutes prêtes.
- Nous aurons besoin plus tard d’ajouter des animations et une mise en page un peu complexe. Pour cela, il y a deux extensions qui sont le flex-layout et le [CDK](https://material.angular.io/cdk/categories)

Pour décrire un peu le scénario de notre application finale. On aura une page de connexion sans barre d’outils. Cette page contient 2 champs textes : identifiant et mot de passe. On ajoutera un bouton de “Connexion” également. Nous allons gérer les messages d’erreurs. Lorsque l’utilisera sera authentifié correctement, on lui affichera la barre d’outil avec un menu simple.

Aussi, on veille à ce que l’application nodejs soit lancée. Sinon, on risque de se retrouver avec des surprises incompréhensibles.

Allons dans le dossier de nodejs et tapons ceci pour lancer l’application nodejs

```
cd nodejs
nodemon ./bin/www
```

L’application nodejs tourne par défaut sur le port 3000**.** Notre application angular tourne quand à lui sur le port 4200 (*en mode développement*).

Mais dans cet article, nous allons nous contenter d’afficher seulement la page de connexion.

# Ajout de Angular Material

Dans le dossier de angular, installons les packages utiles comme ceci

```
npm install --save @angular/material @angular/cdk @angular/animations hammerjs @fortawesome/fontawesome-free @angular/flex-layout
```

Voici les versions de ces packages une fois installées

- [@angular/material](http://twitter.com/angular/material)@8.0.1
- [@angular/cdk](http://twitter.com/angular/cdk)@8.0.1
- [@angular/animations](http://twitter.com/angular/animations)@8.0.2
- hammerjs@2.0.8
- [@fortawesome/fontawesome-free](http://twitter.com/fortawesome/fontawesome-free)@5.9.0
- [@angular/flex-layout](http://twitter.com/angular/flex-layout)@8.0.0-beta.26

Nous avions besoin d’installer [**hammerjs**](http://hammerjs.github.io/) car quelques composants nécessitant la fonctionnalité “touch gesture” de Material dépendent de hammerjs

Nous ajoutons volontairement aussi [font-awesome](https://fontawesome.com/how-to-use/on-the-web/setup/using-package-managers) car on aime bien ces icones supplémentaires.

[angular/flex-layout](https://github.com/angular/flex-layout) est utile lors de la mise en page d’éléments.

Modifions le fichier src/app/app.module.ts et ajoutons

```
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
```

Puis ajoutons dans la directive NgModule

```
@NgModule({
  imports: [BrowserAnimationsModule]
})
```

## Ajout du thème material

Dans src/styles.scss, ajoutons

```
$fa-font-path : '../node_modules/@fortawesome/fontawesome-free/webfonts';
@import '../node_modules/@fortawesome/fontawesome-free/scss/fontawesome';
@import "~@angular/material/prebuilt-themes/indigo-pink.css";
```

# Ajout de hammerjs

Ajout l’appel dans src/main.ts

```
import 'hammerjs';
```

# Ajout des icones material

Pour cela, modifions le fichier src/index.html et ajoutons dans la balise <head></head>

```
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
```

# Créons une page login

Grâce à [Angular/cli](https://angular.io/cli/generate), nous pourons facilement générer une application, une page, un service, une directive, une interface, une class…. Et ceci avec la commande “**ng g** <schematic>**”** ou “**ng generate** <schematic>”

Pour garder la même convention , nous créons la page login dans le dossier src/app/components. Voici la commande pour créer cette page.

```
ng g component components/login
```

Cette commande fait les actions suivantes :

- Crée le fichier src/app/components/login/login.component.html
- Crée le fichier src/app/components/login/login.component.spec.ts
- Crée le fichier src/app/components/login/login.component.ts
- Crée le fichier src/app/components/login/login.component.scss
- Ajoute le component dans src/app/app.module.ts

Regardons de plus près ce qui a été fait dans src/app/app.module.ts

La ligne suivante a été ajoutée

```
import { LoginComponent } from './components/login/login.component';
```

LoginComponent a été ajoutée dans

```
@NgModule({
  declarations: [
    LoginComponent
  ]
})
```

## Maintenant qu’on a notre page, comment y accéder via le navigateur ?

Et bien j’aimerais bien taper http://localhost:4200/#/login et voir la page login que angular a créée.

Et ben en faisant ça, il ne se passe rien.. je suis redirigé vers la page d’accueil

Pallions à ça en modifiant le fichier src/app/app-routing.module.ts

Ajoutons l’import du component login

```
import { LoginComponent } from './components/login/login.component';
```

Dans routes, ajoutons loginComponent comme ceci

```
const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
      path: 'login',
      component: LoginComponent
    }
];
```

Voyons maintenant le résultat de http://localhost:4200/#/login

Gagnééééé

![img](/images/angular-electronjs/1soD1HgZa_l4AHZbb-xleJA.png) 

C’est maintenant dans cette page que nous allons créer notre formulaire de connexion.

Modifions le template src/app/components/login/login.component.html

```
<div fxLayout="row" fxLayoutAlign="center" class="login-main">
  <mat-card >
    <mat-card-header>
      <mat-card-title>{{ 'PAGES.login.header' | translate }}</mat-card-title>
    </mat-card-header>

    <mat-card-content fxLayout="column">
      <mat-form-field>
        <input matInput placeholder="{{ 'PAGES.login.email_placeholder' | translate }}">
      </mat-form-field>

      <mat-form-field>
        <input type="password" matInput placeholder="{{ 'PAGES.login.password_placeholder' | translate }}">
      </mat-form-field>
    </mat-card-content>

    <mat-card-actions align="end">
      <button mat-raised-button color="primary">{{ 'PAGES.login.button' | translate }}</button>
    </mat-card-actions>
  </mat-card>
</div>
```

Ajoutons les composants material dans src/app/app.module.ts. On importe flex-layout et les composants material

```
import { FlexLayoutModule } from '@angular/flex-layout';
// Material
import {
  MatSidenavModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatInputModule,
  MatFormFieldModule,
  MatButtonModule,
  MatListModule } from '@angular/material';
```

Ensuite, on ajoute dans

```
@NgModule({
// ...
  imports: [
    // Material
    MatSidenavModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    FlexLayoutModule
  ]
})
```

Faisons un peu de mise en page. Modifions le fichier src/app/components/login/login.component.scss

```
.login-main {
  margin-top: 2em;
}

.mat-card {
  width: 60%;
}
```

Puisque nous avons bien fait les choses et avons mis les clés de traductions dans le template html. Allons remplir ces traductions dans src/assets/i18n/en.json. On aura ceci

```
{
  "PAGES": {
      "HOME": {
          "TITLE": "Welcome "
      },
      "login": {
        "header": "Login ",
        "email_placeholder": "Votre identifiant",
        "password_placeholder": "Votre mot de passe",
        "button": "Se connecter"
      }
  }
}
```

Actualisons maintenant la page

![img](/images/angular-electronjs/1rNwE5CFRQOGhBwgKGz0BCA.png)

Et voilà.

Sources: https://github.com/rabehasy/angular-electron/tree/step1

Ce post est également publié sur **[Medium](https://medium.com/@miarirabs/angular-electronjs-page-de-connexion-e6f1af452fcb)**