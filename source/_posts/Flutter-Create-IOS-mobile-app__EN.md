---
title: Create an IOS mobile app with Flutter
tags:
  - flutter
  - ios
  - dart
categories:
  - [flutter]
permalink: flutter-fr-ios-get-started
date: 2019-06-14 00:40:07
---

![img](/images/ios/0PfXjCEM_KyG6phyU.png)

After a first introduction on creating a first application with Android, we are going to do the same manipulation and this time with IOS.

Creating an application with IOS itself is easy. There is no difference with Android.

This article will be short and will focus instead on my experience. To summarize, the least obvious step was the deployment of the application on a “real device”, in this case the iPhone.

## Prerequisites for deployment on a device

### xcode

Xcode is the must-have software if you want to run an application on an iphone or ipad.

Go to https://developer.apple.com/develop/ and download Xcode. We also need to create an Apple developer account if we don’t already have one. Registration is free. Having a developer account is required to test an application on an iPhone.

### Homebrew

[Homebrew ](https://brew.sh/index_fr)is the macOS package manager just like [Chocolatey](https://chocolatey.org/) for windows or [aptitude](https://doc.ubuntu-fr.org/aptitude) for linux. It will be useful to install a few bookstores for communicating our application to IOS.

Open a terminal and type the following command to install Homebrew

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Once Homebrew is installed, we can install the various useful packages.

Let’s get this set up.

```
brew update 
brew install — HEAD usbmuxd
brew link usbmuxd
brew install — HEAD libimobiledevice
brew install ideviceinstaller
brew install cocoapods
```

usbmuxd , libimobiledevice and ideviceinstaller are libraries to communicate with IOS natively >> http://www.libimobiledevice.org/

[Cocoapods ](https://cocoapods.org/)is a package manager for Swift and Objective-C. These are the programming languages often used to create an IOS application.

```
pod setup
```

## Let‘s install Flutter

We will download the SDK for mac here https://flutter.dev/docs/get-started/install/macos

![img](/images/ios/11KKismcmOcQRDBpWs1Sykw.png)

Create the /Users/mr/flutter folder and extract the downloaded zip from this folder.

We create or add an entry in our PATH environment variable that will have the following value: **/Users/mr/flutter/bin**

In the terminal, we type this

```
export PATH="$PATH:/Users/mr/flutter/bin"
```

Test for proper operation by typing

```
echo $PATH
```

## Check flutter install before creating the application

```
flutter doctor
```

We must have the following information in our terminal

```
[✓] Flutter (Channel stable, v1.5.4-hotfix.2, on Mac OS X 10.13.6 17G7024,
 locale fr-FR)
[✗] Android toolchain — develop for Android devices
 ✗ Unable to locate Android SDK.
 Install Android Studio from:
 https://developer.android.com/studio/index.html
 On first launch it will assist you in installing the Android SDK
 components.
 (or visit https://flutter.dev/setup/#android-setup for detailed
 instructions).
 If the Android SDK has been installed to a custom location, set
 ANDROID_HOME to that location.
 You may also want to add it to your PATH environment variable.
[✓] iOS toolchain — develop for iOS devices (Xcode 10.1)
[!] Android Studio (not installed)
[✓] Connected device (1 available)
! Doctor found issues in 2 categories
```

So far, we’re right because we don’t need Android.

## Let’s create our 1st app

In the terminal, we go to a folder for example /Users/mr/flutterapps and type this:

```
flutter create firstapp
```

That this task makes :

- It creates the firstapp folder
- In the firstapp folder, it creates an application that will use the Material Components.

## Let’s roll this app…on our iPhone

I will first quote the versions of the tools used in this demo:

- xcode: 10.1
- IOS: 12.3.1

We run Xcode and open the file **Runner.xcodeproj** at the root of the project.

Then in the left pane of Xcode, click Runner

![img](/images/ios/1-8yZF736iYK9nVznvRVBEA.png)

In the middle column and in the Status field, a red icon informs us that something is missing

![img](/images/ios/1zDqIlP9WF1ZpJgd_80_V4w.png)

In this field, we add our developer account. We choose our name in the **Team**field.

![img](/images/ios/1QJvifvJ5DIvHNbhTahZDrw.png)

2 other red icons appears.

![img](/images/ios/1dUJwJ8DvljCBqXQjORxCdg.png)

Because the field identifier “Bundle Identifier” **com.example.firstapp** has the default value and this identifier has already been used by another application in Apple Store (probably the first flutter application;). We’ll just change the value of the “Bundle Identifier” field to **com.flutter.miary** as on the capture and the problem is resolved.

![img](/images/ios/1U6DpkOQUx5SJSBq1KPIYHw.png)

We’re OK on Xcode. We go into the iPhone settings to prepare him to accept the application once the “launch” button is clicked from Xcode.

We go to Settings

![img](/images/ios/1be1KBSOhcQ5UzSpaiVWM3w.png)

… then on Device Management

![img](/images/ios/1JOgM3U8w4r6-GZnACWAasw.png)

…click on our name

![img](/images/ios/1CMLTc96bLsnUF1hRdvTY9w.png)

We go back to Xcode. We will now launch the application from Xcode to our phone. We choose our phone from the list of devices offered in Xcode

![img](/images/ios/19yBq_NngqNQMLgO6VNTmbQ.png)

Then on the icon “ Build “

![img](/images/ios/1YGTl6fV8F7f_c456ppywJw.png)

In our iPhone, we authorize the installation of the application by clicking on “Trust moncompte@dev”

![img](/images/ios/1ZiITg7gApXeAXPctgRZa2A.png)

Click on “YES” in the popup

![img](/images/ios/10HpUC6eMePQtbuEL8DYv_Q.png)

And now we have our app launched in our phone

![img](/images/ios/1A1vIXnwDaOXybtCXOHFOVQ.png)

This post is published on **[Medium](https://medium.com/@miarirabs/create-an-ios-mobile-app-with-flutter-9824982c7189)** too