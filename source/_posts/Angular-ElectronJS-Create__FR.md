---
title: Créer une application web et un logiciel de bureau avec Angular et electronJS
tags:
  - angular
  - electronjs
  - git
  - nodejs
categories:
  - [angular]
  - [electronjs]
permalink: angular-electronjs-get-started
date: 2019-06-16 00:40:07
---

![img](/images/angular-electronjs-get-started/12CGUAKR_kqP54k9IGO_5xA.png)

Prerequis :

- nodejs
- git
- @angular/cli

# Installation des prerequis

## Nodejs

On installe nodejs. On va sur https://nodejs.org/en/ et on télécharge l’installateur.

![img](/images/angular-electronjs-get-started/1zx4jcuwwpQEJVgGvFdWITA.png)

J’ai pris la version “**12.4.0 Current**”

On verifie l’installation.

```
node -v
v12.4.0
```

## Git

On installe git. On va sur https://git-scm.com/

![img](/images/angular-electronjs-get-started/1vPWSyVHitV_z9_uBYOxILg.png)

On verifie la bonne installation

```
git version
git version 2.22.0.windows.1
```

## Angular/cli

*Nodejs est obligatoire pour installer angular/cli.*

On installe la dernière version de Angular grâce à npm (node package manager)

```
npm install @angular/cli
ng version
```

![img](/images/angular-electronjs-get-started/1M6oCEtJykenXEIItvQhJmQ.png)

### Angular-electron

On va faire une copie (git clone) de l’excellent travail de [Maxime GRIS](https://www.maximegris.fr/) .

```
git clone https://github.com/maximegris/angular-electron.git
```

On va dans le dossier de notre application (en l’occurence angular-electron) et on installe les dépendances

```
cd angular-electron
npm install
```

On lance l’application web par défaut dans un navigateur grâce à la commande suivante

```
npm run ng:serve:web
```

L’url devrait être accessible à l’adresse suivante http://localhost:4200/#/ .  

Pour lancer le logiciel de bureau en mode developpement. On tape la commande suivante :

```
npm start
```

Une fenêtre se lance et on obtient ceci

![img](/images/angular-electronjs-get-started/12CGUAKR_kqP54k9IGO_5xA.png)

Pour compiler l’application web pour pouvoir être distribué sur un serveur, on fait

```
npm run build:prod
```

Cette commande va compiler l’application web en mode **— aot** . Je vous renvoie à l’excellent article de [Sébastien Olivier](https://sebastienollivier.fr/blog/angular/angular-jit-vs-aot) qui explique l’utilité de ce mode de compilation **aot (ahead of time)**

Une fois l’application compilée, il suffit de déplacer l’ensemble des fichiers dans le dossier dist/ vers un répertoire FTP par exemple.

La création d’une application executable quand à elle est très facile également. Elle peut prendre un certain temps. Ce qui est normal. Elle se fait grâce à la commande suivante :

```
npm run electron:windows
```

Dans le dossier release/

![img](/images/angular-electronjs-get-started/1VdSGWTRQuApaNYs-0dgqmA.png)

Sources: https://github.com/rabehasy/angular-electron/tree/start

Publié sur **[Medium](https://medium.com/@miarirabs/cr%C3%A9er-une-application-web-et-un-logiciel-de-bureau-avec-angular-et-electronjs-88dec072f799)**