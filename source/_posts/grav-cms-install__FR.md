---
title: Grav cms - installation en quelques secondes
tags:
  - getgrav
  - cms
categories:
  - Tips-Tricks
permalink: grav-cms-install-fr
date: 2019-11-01 21:00:00
---

# CMS

Un CMS (Content Management System) ou *Système de gestion de contenu en ligne* est un outil de travail. Il permet de gérer facilement le contenu d'un site Internet *à partir d'une interface d'administration protégée par un mot de passe et un identifiant.*

Il existe 3 types de CMS :

- **CMS couplé (*ou classique*)**  : le back-office  (pour gérer le contenu) se trouve sur le même serveur que le front-office (site internet). Ils ***doivent*** utiliser le même langage de programmation (php par exemple) et la base de données (MySQL) pour stocker le contenu.
- **CMS découplé (*Decoupled CMS*)** : le back-office (pour gérer le contenu) ne se trouve pas sur le même serveur que le front-office (site internet). Ils **peuvent** utiliser chacun un langage de programmation différent et le front-office n'a pas besoin de savoir la base de données utilisée par le back-office. Les 2 applications communiquent via une API.
- **CMS sans-tête (*Headless CMS*)** - ne possède bas de back-office. Le back-office peut utiliser un langage de programmation différent du front-office. Et celui ci n'a pas besoin de savoir  la base de données utilisée par le back-office. Les 2 applications communiquent via une API restful.

Parmi les CMS couplés, on peut citer (selon l'objectif et la spécialisation du cms) :

- Wordpress - Dotclear  : pour la gestion d'un blog.
- TYPO3 - Wordpress - Drupal - Joomla - SPIP - Contao - concrete5, cmsmadesimple, modx ... : pour la gestion de n'importe quel type de site.
- Magento  - prestashop : pour la gestion d'une boutique en ligne.
- PunBB - phpBB : pour la gestion d'un forum de discussion.
- Mediawiki - pour la gestion des contenus wiki (*utilisé par wikipedia*)
- etc...

# Grav

![](/images/grav/logo.png)

[Getgrav](https://getgrav.org/) est un CMS couplé. Il est Open Source. Il est développé en php. php7.1.3 est requis. 

Cependant, Il est différent d'un CMS couplé classique dans le sens ou c'est un CMS sans base de données. D'ou son classement dans les **[CMS Flat file](https://www.cmscritic.com/flat-file-cms/)** . Les données sont stockés sous forme de fichiers. 

Dans le cas de Getgrav, elles sont stockées au format **yml**.

Cela a plusieurs avantages au dela d'être un CMS couplé :

- Facilité de la maintenance et de la sauvegarde
- Rapidité d'execution des pages
- Moins vulnérable aux failles de sécurité de la base de données.
- Adapté aux petites structures
- Répond au besoin des sites hébergés sur des serveurs mutualisés sans base de données.

Et quelques inconvénients :

- L'écriture de données sur le disque dur est moins rapide que l'écriture en Base de données.
- Le poids des fichiers confondus est plus lourd .

Nous allons décrire pas à pas l'installation de ce cms.

## Installation

Nous allons utiliser `composer` pour installer grav.

Ouvrons un terminal et allons dans un dossier. Puis lancons la commande suivante :

```
cd /var/www/
composer create-project getgrav/grav grav
```

La commande précedente installe **grav** dans le dossier suivant : `/var/www/grav`.

Ensuite il faut aller dans le dossier de grav et lancer :

```
cd /var/www/grav
./bin/grav install
```

Il suffit alors de déposer l'ensemble des fichiers contenus dans le dossier grav sur un serveur et aller à l'url :  http://gravdemo.localhost/  

Et voici le rendu final :

![](/images/grav/demo.png)







