---
title: Créer une application nodejs/express grâce à express-generator
tags:
  - nodejs
  - expressjs
  - expressgenerator
categories:
  - [nodejs]
  - [expressjs]
permalink: express-generator-nodejs-application
date: 2019-06-16 00:40:07
---

Prerequis :

- nodejs

## Installation des prerequis

### Nodejs

On installe nodejs. On va sur https://nodejs.org/en/ et on télécharge l’installateur.

![img](/images/nodejs-express/1zx4jcuwwpQEJVgGvFdWITA.png)

J’ai pris la version “**12.4.0 Current**”

On verifie l’installation.

```
node -v
v12.4.0
```

### Express-generator

Express-generator est un outil tout en un qui permet de créer rapidement une application node-js en utilisant la librairie express. Il sera possible grâce à ce générateur de spécifier le template (view), le preprocesseur CSS…

On veut installer express-generator. On ouvre une console et on tape

```
npm install express-generator -g
```

Pour installer express-generator de façon globale. Ce qui veut dire que plus tard si on le veut, on pourra créer une application depuis n’importe quel dossier. On vérifie la bonne installation de exrpess-generator en affichant la version installée sur notre machine. Pour cela, tapons ceci :

```
express — version
4.16.1
```

### Création de l’application

```
express — view=hbs — css=sass — git
```

Cette commande par exemple, permet de créer une application qui demande de :

- utiliser le template hbs ([handlebars](https://handlebarsjs.com/)).
- configurer [Sass](https://sass-lang.com/) comme processeur css.
- Ajouter un fichier .gitignore à la racine du projet.

Plus d’informations sur cette commande et les possibilités offertes par le générateur : https://expressjs.com/fr/starter/generator.html

On installe ensuite les dépendances suivantes

![img](/images/nodejs-express/1h3yjVd-1DE9dOKOIdQKQdw.png)

grâce à la commande suivante toujours dans le même dossier :

``` 
npm install
```

Voici l’arborescence finale après l’installation des dépendances.

![img](/images/nodejs-express/1MOUzK8LSmuBw2V7NMJS9yA.png)

On lance ensuite l’application en tapant

```
DEBUG=nodejs-express:* npm start
```

On ouvre ensuite dans notre navigateur l’URL suivante : http://localhost:3000/

Et voilà

![img](/images/nodejs-express/1V314FVj7SwvIRqftob8b6w.png)

## Et si…

### 1 — On ne veut pas executer à chaque fois la commande précédente dès qu’on modifie un fichier ?

C’est ce qu’on appelle le “**livereload**” . On voit comment on fait cela. Dans la console, on tape ceci pour installer la librairie **nodemon.** — save-dev ajoute la librairie en mode développement uniquement. Ce qui veut dire que la librairie ne sera pas compilée avec l’application en prod.

```
npm install nodemon — save-dev
```

On ajoute une entrée au niveau de **scripts** dans le fichier package.json

```
"startd": "nodemon ./bin/www"
```

![img](/images/nodejs-express/1cOIFpvkJPiDleXW4zO-iBA.png)

On va tester en modifiant un fichier — par exemple nodejs-express/routes/index.js

On change

```
res.render('index', { title: 'Express' });
```

en

```
res.render('index', { title: 'Madatsara' });
```

On rafraichit la page et on observe la magie.

### 2 — on voulait utiliser du scss plutot que du sass

Dans nodejs-express/app.js

On modifie la ligne **indentedSyntax**

```
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: false, // true = .sass and false = .scss
  sourceMap: true
}));
```

et on ajoute un fichier **scss** dans nodejs-express/public/stylesheets/style.scss

Sources : https://github.com/rabehasy/nodejs-express/tree/start