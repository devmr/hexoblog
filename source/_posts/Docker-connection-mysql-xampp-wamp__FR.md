---
title: Docker - localhost de xamp (wamp) à host.docker.internal
tags:
  - docker
categories:
  - - Tips-Tricks
  - - docker
permalink: docker-connection-mysql-xampp-wamp-fr
date: 2019-08-18 20:40:07
---

---

# Contexte

Pour pouvoir monter un serveur AMP (Apache MySQL et PHP) sur windows, j'abandonne xampp (ou wamp) pour **docker**.  

## Qu'est-ce que Docker ?

La meilleure définition de docker que j'ai trouvée à la fois simple et clair est celle-ci : "*Logiciel open source permettant de créer, deployer des containers d'applications virtualisés sur un système d'exploitation.*"

{% googleads 'in-article' 'fluid' '8275635211' %}

# Problématique

J'ai la contrainte suivante . Plusieurs bases de données ont déja été créées avec xampp. Le poids de l'ensemble des bases de données fait plus de 2Go (*Autant dire que cela prendra du temps pour les importer sur un autre serveur*.)

Cependant, je peux opter pour :

- un `mysqldump` dans xampp et un import de ce dump dans Docker.
- Une copie de tous les dossiers dans `C:\xampp\mysql\data` sans oublier les fichiers `ib_logfile*` et `ibdata1` de xampp vers le dossier de docker.

Je ne vais pas choisir ces 2 solutions.

# Contournement du problème

Je vais garder les bases de données dans xampp. Je vais juste changer le nom de l'appel de `localhost`dans les scripts utilisés avec Docker à `host.docker.internal`.

Modifions le fichier hosts C:\windows\system32\drivers\etc\hosts et ajoutons la ligne avec l'adresse IP de sous-réseau 

```
192.168.0.1    host.docker.internal
```

Ensuite à l'endroit où on fait appel à une connexion à la BDD depuis un script, on modifie en général `localhost` ou `127.0.0.1`par `host.docker.internal` .

Et c'est tout. Il ne nous reste plus qu'à construire notre image si ce n'est pas déja fait

```
docker-compose build
```

et lancer (deployer)

```
docker-compose up -d
```

Ma contrainte restante - il faut lancer uniquement **mysql** dans xampp.