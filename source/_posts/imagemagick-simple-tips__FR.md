---
title: Utiliser ImageMagick pour faire du Traitement d'image en masse (flou gaussien, fond opaque , texte centré dans l'image)
tags:
  - magick
  - imagemagick
  - cli
categories:
  - [Tips-Tricks] 
permalink: image-magick-tips
date: 2020-03-01 20:40:07

---

![And Now a Touch of Magick](/images/magick.jpg)

# Présentation

[Imagemagick](https://imagemagick.org/index.php) est un programme qui permet de traiter une image comme tout autre logiciel de traitement d'image tel que : Photoshop, Gimp...et ceci gratuitement.

Habituellement, on l'utilise en ligne de commande. 

Parmi les **[possibilités offertes](https://imagemagick.org/index.php#features)** par le programme, on peut citer :  

- La gestion des couleurs 
- Le mélange de plusieurs images.
- L'ajout de bordures à une image
- Le dessin sur une image : ajouter des formes (cercles, carrés...)
- La conversion d'un format vers un autre : jpeg --> png...
- La gestion des dégradés.
- Le montage de plusieurs images.
- L'ajout d'effets spéciaux: flou , teinte, seuil
- L'ajout de texte à une image.
- La transformation: redimensionnement, rotation, recadrer, redressement, retournement
- et bien d'autres ...

En ligne de commande, jusqu'à la version 6, on se sert de la commande `convert` pour utiliser ce programme.

Cela  a changé depuis la version 7. Désormais, on utilise maintenant la commande `magick convert` ou tout simplement `magick`

# Objectif

Notre objectif est ici :

- [*Optionnel Installer magick 7*]
- Pour toutes les images présents dans un dossier :
  - Ajouter un **effet de flou** à l'image.
  - Ajouter un **fond blanc *transparent*** par dessus chaque image
  - Ajouter  un **texte en noir *centré*** dans l'image avec la possibilité de choisir la **police** de caractère.
  - **Enregistrer** l'image dans un dossier. 

Ces étapes peuvent très bien être faites à partir d'un logiciel de traitement d'images en créant un script par exemple. Mais imaginons le cas ou nous devons faire ceci sur des images présents dans un dossier sur un serveur. 

💡 Ok tu vas me dire -  il suffit de le lancer le script sur mon PC et envoyer ensuite sur le serveur les fichiers traités.

Ce à quoi je te réponds - Oui en effet - Cependant, si ces images sont dynamiques ? c'est-à-dire envoyées par les internautes . Tu vas vite te rendre compte que cela va te faire perdre énormément de temps. 

💡 .. Et tu te dis à ce moment là : j'aurai du utiliser une ligne de commande sur le serveur pour faire un traitement automatique . Je créerai aussi un crontab pour lancer périodiquement cette tâche - comme çà je n'ai plus rien à faire. 

Et bien - c'est exactement ce que je te propose  de faire et c'est ce que je vais détailler dans la suite.

## Installation de imagemagick (optionnel)

Cette étape est optionnelle si tu as déja imagemagick installé sur ton poste. Tu peux directement passer à l'étape **[Script]()**

Je vais te détailler 2 installations :

- Windows 10
- Debian 9 Stretch.

### Windows  10

Je me sers de **[chocolatey](https://chocolatey.org/)** pour installer les logiciels sur mon PC. C'est un gestionnaire de package (comme apt, brew) sur Windows.

Je ne vais pas te détailler l'installation de ce logiciel puisque tout est très bien expliqué ici https://chocolatey.org/install 

💡 *Tu n'es pas obligé d'utiliser chocolatey pour installer ImageMagick hein :) Tu peux très bien suivre les étapes décrites ici https://imagemagick.org/script/download.php*

Sur le site de chocolatey, tu tapes `imagemagick` dans la barre de recherche. Une fois le package trouvé, le site te propose une commande à copier. Il s'agit de 

`choco install imagemagick`

Tu mets ça dans ton terminal ( j'utilise **[ConEmu-Maximus5](https://conemu.github.io/)** ). Je te conseille aussi d'executer le terminal en tant qu'**Administrateur**.

Des questions te seront posées dans le terminal. Il te suffit d'accepter chaque question en tapant **Y**  ou **A** si tu choisis d'accepter toutes les questions posées par la suite.

### Linux avec Debian 9 stretch

Je te résume les commandes suivantes, il se peut que t'aies pas besoin de la 2ème commande `apt-get install build-essential` par exemple car lepackage est déjà installé sur ton serveur. 

En résumé, ces commandes suivent les étapes suivantes :

- Téléchargement du code source .targ.gz de imagemagick dans un dossier /tmp/
- Décompression du fichier téléchargé .tar.gz
- Installation du logiciel à partir du dossier décompréssé

```shell
apt-get update
apt-get install build-essential
cd /tmp/
wget https://www.imagemagick.org/download/ImageMagick.tar.gz
tar xvzf ImageMagick.tar.gz
cd ImageMagick-7.0.10-1/
./configure
make
make install
ldconfig /usr/local/lib
```

### Test

Pour savoir si imagemagick est bien installé, tu peux lancer la commande suivante

```
magick -version 
```

Ce qui te retournera ceci par exemple

Sur **Windows 10**

```
Version: ImageMagick 7.0.8-62 Q16 x64 2019-08-24 http://www.imagemagick.org
Copyright: Copyright (C) 1999-2018 ImageMagick Studio LLC
License: http://www.imagemagick.org/script/license.php
Visual C++: 180040629
Features: Cipher DPC Modules OpenCL OpenMP(2.0)
Delegates (built-in): bzlib cairo flif freetype gslib heic jng jp2 jpeg lcms lqr lzma openexr pangocairo png ps raw rsvg tiff webp xml zlib
```

et sur **Debian 9**

```
Version: ImageMagick 7.0.10-1 Q16 x86_64 2020-03-21 https://imagemagick.org
Copyright: © 1999-2020 ImageMagick Studio LLC
License: https://imagemagick.org/script/license.php
Features: Cipher DPC HDRI OpenMP(4.5)
Delegates (built-in): bzlib fontconfig freetype jbig jng jpeg lcms lzma png tiff webp wmf x xml zlib
```

## Script

Je vais maintenant créer un script php, le nommer `scriptmagick.php`et ensuite l'executer dans un dossier.

Ce script accepte 2 arguments :

- le dossier source : contient les images sources
- le dossier de destination : contiendra les images traitées

Il sera executé comme ceci après m'être positionné dans le dossier parent de "source/" et "destination/". C'est à l'intérieu r de ce script que je vais ajouter les commandes `magick`.

```
php "scriptmagick.php" "source/" "destination/"
```

![image-20200322134640467](/images/magick-folders.png)

Voici les images qui vont être traitées

![image-20200322134848055](/images/image-20200322134848055.png)

On remarque que ces images ont des dimensions différentes

![image-20200322135022263](/images/image-20200322135022263.png)

Procédons par étape, nous devons :

- Parcourir chaque image dans le dossier "source" 
- Pour une image :
  - récupérer la dimension de l'image.
  - Appliquer un effet de flou à l'image et enregistrer le rendu dans un fichier temporaire.
  - Appliquer un fond blanc transparent à cette image.
  - Ajouter un texte : "VENDU" coloré en noir au centre de l'image
  - Enregistrer le fichier final dans le dossier "destination"

### PHP

Avant de créer le script, je vais installer une dépendance utile à l'utilisation de php à partir d'un terminal. Il s'agit du composant **symfony/process**. Comme j'ai déja **[composer](https://getcomposer.org/)** installé, il suffit que j'execute cette commande dans un dossier.

```
composer require symfony/process
```

Je crée un fichier "src/scriptmagick.php" dans ce dossier.

![image-20200322141719387](/images/image-20200322141428442.png)

Voici le code source de ce fichier

{% gist a3b01c8e9435537e9da1a83b65789233 scriptmagick.php %}

De plus, je crée 2 fichiers qui permettent de gérer les exceptions :

- src/ArgsmissingException.php
- src/SourcemissingException.php

Avec un code source très simple

{% gist b8f5fa9e20fb012203859d09d827a4a0 ArgsmissingException.php %}

{% gist f36f0f97a5eb8fb4d6124a61b6f58868 SourcemissingException.php %}

J'execute ce fichier comme ceci

```shell
php src/scriptmagick.php "C:\source" "C:\destination"
```

Et dans mon dossier "destination/" , voici le rendu final des images traitées.

![image-20200322155912655](/images/image-20200322155912655.png)









