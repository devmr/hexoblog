---
title: Android Studio - Could not find aapt2-proto.jar après Upgrade
tags:
  - android
categories:
  - - Tips-Tricks
permalink: android-error-could-not-find-aapt2-proto-fr
date: 2019-09-07 21:00:00
---


# Contexte

Android Studio ne veut plus  construire mon application depuis que j'ai mis à jour Android studio en version 3.5.

Je me retrouve avec l'erreur suivante :

```
ERROR: Could not find aapt2-proto.jar (com.android.tools.build:aapt2-proto:0.3.1).
Searched in the following locations:
    https://jcenter.bintray.com/com/android/tools/build/aapt2-proto/0.3.1/aapt2-proto-0.3.1.jar
```

# Solution

Je modifie :

- build.gradle

Puis dans

```
repositories {
}
```

Je fais passer `google()` avant `jcenter()`comme ceci

```
repositories {
        google()
        jcenter()
    }
```

Je vide les caches et je redémarre en allant dans `File &gt; invalid cache and restart`



