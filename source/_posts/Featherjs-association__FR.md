---
title: 'Featherjs  - jointures - BelongsTo, hasOne, hasMany'
tags:
  - featherjs
  - nodejs
  - sequelize
categories:
  - [featherjs]
  - [sequelize]
permalink: featherjs-association-fr
date: 2019-07-22 00:40:07
---



Cet article fait d'une série d'article sur  {% post_link featherjs-create-api-rest-fr 'Feathersjs' %} . Nous avons précédemment abordé les points suivants :

- {% post_link featherjs-create-api-rest-fr 'C.R.U.D (Create Read Update Delete)  avec une base de données MySQL en utilisant l\'ORM Sequelize' %}
- {% post_link featherjs-authentication-upload-fr 'Authentification jwt et protection d\'une ressource (URL)'  %}
- {% post_link featherjs-upload-fr 'Envoi de fichier vers un serveur avec multer'  %}

Dans cet article, nous allons voir comment faire une liaison entre plusieurs modèles. 

{% googleads 'in-article' 'fluid' '8275635211' %}

# Association

Pour rappel, chaque événement (table `event`)  enregistré dans notre base de données :

- est lié à une source. Ce qui nous permet de savoir de quelle façon l'événement a été enregistré. Nous avons 3 types de sources enregistrées dans la table `api` :
  - **Android** : événement créé grâce à l'application android avec Flutter
  - **Desktop**: événement créé en utilisant l'application de bureau Electronjs
  - **Web**: événement créé grâce à l'application web accessible via une URL à partir d'un navigateur connecté à Internet.
- Possède une date. Cette date :
  - peut être unique.
  - ...ou entre 2 intervalles : du ... au ..
  - est multiple : exemple: samedi 15/06, samedi 22/06, dimanche 23/06...
- Possède un ou plusieurs flyer
- Est lié à un ou plusieurs 
  - artiste(s)
  -  lieu(s).
  - autres événement(s).
  - événement spécial : exemple: *événements du réveillon du nouvel an*.
- Est classé dans une thématique - exemple: "Sorties/Musique" - enregistré dans la table `event_type`
- l'accès à l'événement est : payant", "gratuit", "reservé moyennant inscription" ... - enregistré dans la table `entree_type `

Certaines des liaisons citées ci-dessous peuvent être identifiées facilement en observant les champs dans la table `event`. Exemple: 

- le champ `api_id` est un champ de type "Integer" est lié à la table `api`
- thematique - table `event_type`
- entree - table `entree_type`

D'autres non. Ces derniers sont en général des relations de type `many-to-many` : *un événement est lié à plusieurs artistes et un artiste peut être lié à plusieurs événements.*

# JSON brut avec feathers

On veut afficher les informations d'un événement en particulier - cet événement possède l'identifiant  : **5**. 

D'abord, assurons-nous d'être authentifié correctement. 

Pour s'authentifier si ce n'est pas déja le cas (*dans POSTMAN*) :

- Méthode : **POST**

- URL : **http://localhost:3030/authentication/**

- Dans `Body > Raw` , on saisit :

  ```
  {
    "strategy": "local",
    "username": "miary",
    "password": "test"
  }
  ```

La soumission nous retourne un **token**

![](/images/featherjs/loginuser.jpg)

Nous allons ensuite pouvoir afficher l'événement numéro **5**  (*dans POSTMAN*) . 

- Méthode: **GET**
- URL: **http://localhost:3030/api/event/5/**

Voici ce qu'on a

![](/images/featherjs/event5.jpg)

![](/images/featherjs/event52.jpg)

Il s'agit d'une donnée  JSON **brute** . C'est-à-dire tous les champs de la table `event` 

# JSON avec des relations 

Nous allons maintenant ajouter des relations. 

Commencons par la relation avec la table `api`.

La première chose à faire est de modifier le modèle `event` c'est-à-dire le fichier `src\models\event.model.js`

La modification se fera dans la fonction suivante :

```javascript
event.associate = function (models) {

};
```

Un événement `event` est attaché à une seule source `api`. C'est donc une relation de type `belongsTo`

Ajoutons ceci

```
const {  api } = models;
event.belongsTo(api);
```

- Ligne1 : nous avons ajouté le model `api`
- Ligne 2 : nous définissons le type de relation entre `event`et `api`

## Créons un hook `event`

{% googleads 'in-article' 'fluid' '8275635211' %}

Un [hook](http://docs.feathersjs.com/api/hooks.html) featherjs est un ensemble de fonction `middleware` qui s'éxécute avant, après l'éxecution d'une méthode dans un service ou en cas d'erreur lors de l'appel de cette méthode. 

Feathers possède une commande en ligne permettant de créer un hook.  Lorsque l'invite nous demande le nom, nous saisissons `event` 

```
feathers generate hook
```

Le fichier suivant est créé  :  `src\hooks\event.js`. Il contient le code suivant :

```
module.exports = function (options = {}) {
  return async context => { 

    return context;
  };
};
```

Nous ajoutons la relation avec le modèle `api` avant le retour de la fonction. Ce qui donne

```
module.exports = function (options = {}) {
  return async context => { 
	
	const sequelize = context.app.get('sequelizeClient');
    	const { api } = sequelize.models;
    	context.params.sequelize = {
    	include: [
    		{ model: api }
    	], raw: false
    };
    
    return context;
  };
};
```

Après avoir modifié le hook, ajoutons maintenant dans la méthode du hook de `event`. Il s'agit de modifier le fichier `\src\services\event\event.hooks.js`

Ajoutons 

```
const eventHook = require('../../hooks/event');
```

Puis dans `before` puisque nous souhaitons ajouter un hook avant l'appel de la méthode

```
before: {
    get: [eventHook()]
  },
```

Retournons dans POSTMAN

- Méthode: **GET**
- URL: **http://localhost:3030/api/event/5/**

![](/images/featherjs/event53.jpg)

Une entrée `api`a été ajoutée au JSON. Il s'agit bien de la ligne correspondante à l'identifiant **2** dans la table `api`