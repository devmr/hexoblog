---
title: Create an Android mobile app with Flutter
date: 2019-06-12 22:14:13
tags:
  - flutter
  - dart
  - android
categories:
  - [flutter]
permalink: flutter-android-get-started
---

![](/images/Flutter-—-creer-une-application-sous-Android…_images/53f6ff6b.png)

Why did Flutter interest me? On the one hand, because… I wasn’t too excited about ReactNative/NativeScript tools or hybrid frameworks such as Cordova/Xamarin… (though I have a soft spot for Ionic hic;). Also, I know the JAVA Android development but not Swift. By the way, I created a full Android application before the popularity of Kotlin:)

[Flutter](https://flutter.dev/) is an Open Source framework created by Google. You can create a native mobile application under Android and IOS with it.

## …Source

![](/images/Flutter-—-creer-une-application-sous-Android…_images/1fa1f87c.png)

*Flutter was created using the Dart language (itself created by Google). Dart was created to compete with JavaScript, create faster applications and add missing features to JavaScript such as classes, interfaces…*

To get back to the point, I want to create a native mobile app compatible with Android and IOS without having to learn to/and code in 2 different languages (Android/kotlin and swift). The choice of Flutter seemed obvious to me;) In the end, I only focus on my Dart code. The rest of the work is entrusted to Android to make sure my app works.

Flutter is very well documented. A basic Flutter application starts in (barely) 5 minutes.

## Let’s install Flutter

I usually work with Windows. I use a simple text editor such as Sublime text and the command line to develop with Flutter: create an application, launch in a simulator or device, update a package, etc.


Go to https://flutter.dev/docs/get-started/install/windows and download the zip

![](/images/Flutter-—-creer-une-application-sous-Android…_images/77ee6eec.jpeg)

We extract the downloaded zip in : **C:\flutter\bin**

We have the option of doing this manually or on the command line. If we choose to do so on the command line, open a DOS console and type the following command:

{% codeblock %}
PATH %PATH%;C:\flutter\bin {% endcodeblock %}

To check , we type

```
path
```

**C:\flutter\bin** is added to the path list. Do not hesitate to restart the DOS console if necessary (and if it does not work the first time)

## Flutter Check before creating the application

```
flutter doctor
```

We may have the following lines in return

```
[√] Flutter (Channel stable, v1.5.4-hotfix.2, on Microsoft Windows [version 10.0.17134.765], locale fr-FR)
[!] Android toolchain — develop for Android devices (Android SDK version 28.0.3)
 ! Some Android licenses not accepted. To resolve this, run: flutter doctor — android-licenses
[!] Android Studio (not installed)
```

In this case, type this to add Android licenses.

```
flutter doctor — android-licenses
```

At this stage, if Android Studio is not installed on the machine, it will still work. This is not a blocking information so.

## Let’s create our 1st app

Let’s create our 1st app

```
flutter create monAppli
```

What does this command do:

- She creates the monAppli folder
- In this folder, it creates an application that will use the Material
  Components.
  
## Let’s throw this app


Still in the DOS console, we go to the monAppli folder and run

```
cd monAppli
flutter run
```

Let’s watch the magic in our simulator or phone directly while we’re at it;)

Here we go…we created our first app.

![](/images/Flutter-—-creer-une-application-sous-Android…_images/c36f916f.png)

## Let’s modify a little this app


We go to our text editor, and we open the file lib\main.dart.
We’ll change the title. We’re replacing

```
home: MyHomePage(title: ‘Flutter Demo Home Page’),
```

by

```
home: MyHomePage(title: ‘Flutter application MR’),
```

And

```
Text(
 ‘You have pushed the button this many times:’,
 ),
```

by

```
Text(
 ‘Nb de Clic effectué sur ce bouton :’,
 ),
```

In our DOS console, we press the “**r**” key on our keyboard. We look at the result in the app

![](/images/Flutter-—-creer-une-application-sous-Android…_images/c5401213.png)

## What if…

 Because life sometimes has a few surprises in store for us.

### I have several simulators.

*And I want to run the app on all the simulators, including my phone.*

Instead of making Flutter run, we do

```
flutter run -d all
```

Or on a single simulator in the list

```
flutter run -d <deviceid>
```

To get the “deviceid” we do

```
flutter devices
```

Will return

```
1 connected device:
Nokia 2 • E1MGAP27C2249175 • android-arm • Android 7.1.1 (API 25)
```

Then all you have to do is

```
flutter run -d E1MGAP27C2249175
```

to run the application on this device only.

### I don’t want to install Android Studio


while still having the android SDK running

Go through Chocolatey (a package manager on windows)
We launch a Powershell to install Chocolatey. And we type:

```
@powershell -NoProfile -ExecutionPolicy unrestricted -Command “iex ((new-object net.webclient).DownloadString(‘https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin
```

When Choco is installed, we will be able to install JDK and android-sdk with.

```
choco install adoptopenjdk — version 8.192
choco install android-sdk gradle -y
```

Then install the SDK manager (allows to install any version of Android)

```
"%ANDROID_HOME%\tools\bin\sdkmanager" "emulator" "platform-tools" "platforms;android-28" "build-tools;28.0.3" "extras;android;m2repository" "extras;google;m2repository"
```

### Around Flutter

Examples of applications created with Flutter:
https://itsallwidgets.com/

Dart & Flutter packages : https://pub.dev/flutter

{% post_link flutter-android-debuter 'French version ' %}  


{% blockquote Medium https://medium.com/@miarirabs/flutter-create-an-android-mobile-app-778beee61c90 %}
View this article on Medium {% endblockquote %}