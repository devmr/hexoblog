---
title: Featherjs  - connexion MySQL avec Sequelize
tags:
  - featherjs
  - nodejs
  - mysql
  - sequelize
categories:
  - [featherjs]
  - [sequelize]
permalink: featherjs-create-api-rest-fr
date: 2019-07-07 00:40:07
---


![Feathers logo](/images/featherjs/feathers-logo-wide.png)

# Présentation

[Featherjs](https://feathersjs.com) est un framework javascript/nodejs. Il a pour objectif de créer une API RESTful. Il fonctionne avec plusieurs technologies  telles que : [expressjs](https://expressjs.com/) , [socket.io](https://socket.io/) ...

# Prérequis

 Installons nodejs  .

Si ce n'est pas encore fait, l'installation est très simple. Sur Windows, il faut aller sur https://nodejs.org/en/ et télécharger l'installateur

![](/images/nodejs-blog-powered-hexo_images/bbb6e3ea.png)	

On vérifie ensuite la bonne installation de node en tapant ceci dans la console :

```
node -v
v12.4.0
```

# Installation

On ouvre une console. On crée un dossier - par exemple: `featherjs`. On va dans ce dossier

```
mkdir featherjs
cd featherjs
```

Comme tout package nodejs, featherjs peut être installée de façon globale ou dans un dossier de l'application. Chaque méthode a ses inconvénients et avantages.  Dans notre cas, nous l'installons en global. 

L'installation de feathers en global se fait grâce à la commande suivante :

```
npm install -g @feathersjs/cli
```

Une fois installée globalement,  feathers possède un générateur et nous permet  plusieurs choses :

- Créer une application - `app`
- Créer un service - `service`

Dans un premier temps, nous allons créer une application. Saisissons la commande suivante dans notre console  :

```
feathers generate app
```

Une liste de question sera posée. Acceptons les valeurs proposées.

```shell
? Project name featherjs
? Description
? What folder should the source files live in? src
? Which package manager are you using (has to be installed globally)? npm
? What type of API are you making? (Press <space> to select, <a> to toggle all, <i> to invert selection)REST, Realtime via Socket.io
? Which testing framework do you prefer? Mocha + assert
   create package.json
   create config\default.json
   create LICENSE
   create public\favicon.ico
   create public\index.html
   create .editorconfig
   create src\app.hooks.js
   create src\channels.js
   create src\index.js
   create src\logger.js
   create src\hooks\log.js
   create src\middleware\index.js
   create src\services\index.js
   create .gitignore
   create README.md
   create src\app.js
   create test\app.test.js
   create .eslintrc.json
   create config\production.json
...

+ serve-favicon@2.5.0
+ winston@3.2.1
+ helmet@3.18.0
+ cors@2.8.5
+ compression@1.7.4
+ @feathersjs/configuration@2.0.6
+ @feathersjs/feathers@3.3.1
+ @feathersjs/errors@3.3.6
+ @feathersjs/express@1.3.1
+ @feathersjs/express@1.3.1
+ @feathersjs/socketio@3.2.9

```

On lance le serveur qui nous permet de tester featherjs grâce à cette commande

```
npm run dev
```

Dans la console, on a ce  retour

![](/images/featherjs/console-name.jpg)

On ouvre un navigateur et on lance l'url http://localhost:3030/ 

Voici ce qu'on devrait avoir comme résultat

![](/images/featherjs/featherjs.jpg)



 # MySQL avec Sequelize

Nous allons maintenant créer un service qui se connectera à notre base de données MySQL. 

Créons un service à partir de feathers

```
feathers generate service
```

Voici le retour attendu

```
? What kind of service is it? Sequelize
? What is the name of the service? api
? Which path should the service be registered on? /api/api
? Which database are you connecting to? MySQL (MariaDB)
? What is the database connection string? mysql://root:@localhost:3306/featherjs
No locked version found for mysql2, installing latest.
    force config\default.json
   create src\services\api\api.service.js
    force src\services\index.js
   create src\models\api.model.js
   create src\services\api\api.hooks.js
   create test\services\api.test.js
   create src\sequelize.js
    force src\app.js
npm WARN featherjs@0.0.0 No repository field.
npm WARN featherjs@0.0.0 No license field.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.9 (node_modules\fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.9: wanted {"os":"darwin","arch"
:"any"} (current: {"os":"win32","arch":"x64"})

+ feathers-sequelize@4.1.1
added 2 packages from 2 contributors and audited 2890 packages in 19.995s
found 0 vulnerabilities

npm WARN rollback Rolling back readable-stream@2.3.6 failed (this is probably harmless): EPERM: operation not permi
tted, lstat 'F:\data\sites\madatsara.com\featherjs\node_modules\fsevents\node_modules'
npm WARN featherjs@0.0.0 No repository field.
npm WARN featherjs@0.0.0 No license field.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.9 (node_modules\fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.9: wanted {"os":"darwin","arch"
:"any"} (current: {"os":"win32","arch":"x64"})

+ mysql2@1.6.5
+ sequelize@4.44.2
added 28 packages from 111 contributors and audited 2935 packages in 30.026s
```



Comme on peut le voir sur le retour :

- On crée un service nommé  **api** 

- On utilisera **sequelize**.  

- L'api sera accessible via l'URL suivante: `http://localhost:3030/api/api`

  

  # CRUD

  CRUD est un concept largement utilisé en API restful. C'est l'acronyme de **C**reate **R**ead **U**pdate **D**elete. Featherjs implémente nativement ce concept.

  ## READ

  ... pour lire les informations depuis la base de données. 

  

  Voici un extrait de la table `api` que nous allons lire.

![](/images/express-sequelize/1WaSDIeoqtePoUp2MY4MOOA.png)

Dans Postman, nous allons suivre les étapes suivantes :

- Méthode : **GET**
- URL : http://localhost:3030/api/api
- Clic sur le bouton "SEND"

Voici le retour de cette commande

![](/images/featherjs/postman_api_service.jpg)

L'équivalent en requête SQL serait 

```mysql
SELECT * FROM `api`;
```



# CREATE

Nous allons ajouter une nouvelle ligne dans la table. La manipulation à faire est uniquement dans POSTMAN. *Aucune action est requise au niveau du script.*

Dans Postman, On fait les modifications suivantes:

- Méthode : **POST**
- URL : http://localhost:3030/api/api
- Dans body > on saisit `{"name":"new api"}`
- Clic sur le bouton "SEND"

![](/images/featherjs/postman1.jpg)

Suite à cet ajout

![](/images/featherjs/mysql.jpg)

L'équivalent en requête SQL serait 

```mysql
INSERT INTO `api` (`name`) VALUES ('new api');
```



## UPDATE (*avec PUT*)

Modifions cette dernière entrée :

- Méthode : **PUT**
- URL : http://localhost:3030/api/api/12 *(12 étant l'Identifiant de la ligne dans la table)*
- Dans body > on saisit `{"name":"titre modifiée api"}`
- Clic sur le bouton "SEND"

![](/images/featherjs/postman2.jpg)

Observons le résultat

![](/images/featherjs/mysql2.jpg)

L'équivalent en requête SQL serait 

```
UPDATE `api` SET `name` = 'titre modifiée api' WHERE `id` = 12;
```



## UPDATE (*avec PATCH*)

- Méthode : **PATCH**
- URL : http://localhost:3030/api/api/12 (12 étant l'Identifiant de la ligne dans la table)
- Dans body > on saisit `{"name":"titre modifiée en patch api"}`
- Clic sur le bouton "SEND"

![](/images/featherjs/postman2.jpg)

Observons le résultat

![](/images/featherjs/mysql3.jpg)

L'équivalent en requête SQL serait 

```
UPDATE `api` SET `name` = 'titre modifiée api' WHERE `id` = 12;
```



## Mais pourquoi PATCH ?

PATCH et PUT donnent les mêmes résultats en effet dans notre cas. La différence entre ces 2 méthodes est la suivante: PATCH permet une mise à jour partielle. Exemple: mettre à jour uniquement un champ de la table. La méthode PUT quand à elle met à jour tous les champs de la table. 

## DELETE

Et enfin, nous allons supprimer cette ligne.

- Méthode : **DELETE**
- URL : http://localhost:3030/api/api/12 *(12 étant l'Identifiant de la ligne dans la table)*
- Dans body > on saisit `{"name":"titre modifiée api"}`
- Clic sur le bouton "SEND"

![](/images/featherjs/postman4.jpg)

La ligne a bien été retirée de la table

![](/images/featherjs/mysql4.jpg)

L'équivalent en requête SQL serait 

```
DELETE FROM `api` WHERE `id` = 12;
```



# READ avec Query

Revenons un peut sur **READ** et explorons la puissance de featherjs sur les **[Query](https://docs.feathersjs.com/api/databases/querying.html)**. 

Il est commun dans le cadre d'une application web de coder le controller.  

Exemple: si je veux afficher les lignes de la table "**api**" dont le champ "**name**" contient l'expression "**app**" :

- je commence par créer une méthode qui me permettra de gérer ce type de condition. 
- Ensuite, je modifie la requête SQL pour prendre en compte la valeur de mon paramètre. 
- Enfin, j'affiche le résultat attendu.

featherjs nous économise tout ce temps. Tout cela existe nativement. Il faut juste lui donner les bons paramètres et le tour est joué.

Voyons cela de plus près.

Avant toute chose, ajoutons quelques lignes dans notre table `api`. Après ajout, elle devrait ressembler à ceci.

![](/images/featherjs/mysql5.jpg)

## On veut afficher que `$limit` lignes

Il s'agit de limiter le nombre de lignes à afficher en spécifiant un nombre au paramètre `$limit`

La requête SQL ressemble à ceci si on ne voulait afficher que **2 lignes**

```mysql
SELECT * FROM api LIMIT 2;
```

Cela se traduit grâce à featherjs par les manipulations suivantes dans POSTMAN :

- Méthode: GET
- URL : `http://localhost:3030/api/api?$limit=2`
- Clic sur le bouton "SEND"

Résultat

![](/images/featherjs/limit2.jpg)

## `$skip` ou offset

Comme le paramètre `$limit`, ce paramètre indique tout simplement d'**éviter** les **$skip lignes**. 

En SQL on aurait

```mysql
SELECT * FROM api LIMIT 2,2;
```

.. grâce à featherjs dans POSTMAN :

- Méthode: GET
- URL : `http://localhost:3030/api/api?$limit=2&$skip=2`
- Clic sur le bouton "SEND"

![](/images/featherjs/limit2andskip2.jpg)

## On veut trier les résultats avec le paramètre `$sort`

Dans un tri, il est possible de choisir le **sens** :  croissant ou décroissant : **ASC** ou **DESC**

Avec featherjs, il s'agit de donner le nom du champ à trier dans un Array comme ceci

```
$sort[name]=-1
```

Pour :

- trier par champ **name**
- Ordre **decroissant** (*DESC*) qui correspond à l'entier **-1** ou **1** si ordre croissant (*ASC*)

La méthode ne change pas. L'URL devient `http://localhost:3030/api/api?$limit=2&$skip=2&$sort[name]=-1`

![](/images/featherjs/limit2andskip2sortname__desc.jpg)

## Ma table possède plusieurs champ. Je veux juste `$select` quelques champs

Il faut ajouter le paramètre `select[]` autant de fois que je veux choisir les champs à afficher.

Je change l'URL comme ceci `http://localhost:3030/api/api?$limit=3&$sort[name]=1&$select[]=name&$select[]=created_at`

![](/images/featherjs/select.jpg)

## Recherche `$like`

La syntaxe d'une requête pour chercher un texte peut être différent selon le gestionnaire de base de données utilisé. Nous utilisons dans notre exemple une base de données en MySQL. Une recherche se fait donc grâce à l'opérateur `LIKE`comme dans cette requête

```mysql
SELECT `name`, `created_at` FROM api WHERE `name` LIKE '%web%';
```

Cela se traduit par l'url suivante : `http://localhost:3030/api/api?name[$like]=%web%&$select[]=name&$select[]=created_at`

Et voici le résultat

![](/images/featherjs/like.jpg)

 



