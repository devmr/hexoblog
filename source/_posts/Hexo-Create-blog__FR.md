---
title: Créer un blog en nodejs en utilisant hexo
permalink: nodejs-blog-powered-hexo
date: 2019-06-21 23:00:00
updated: 2019-06-27 23:00:00
tags:
  - nodejs
  - hexo
  - markdown
categories:
  - [nodejs]
  - [hexo]
---

![Hexo](/images/nodejs-blog-powered-hexo_images/39572d5d.png)

**[Hexo](https://hexo.io)** est un framework construit avec  nodejs permettant de créer rapidement et facilement un blog. A l'instar de wordpress ou d'autres plateformes de blog, les articles et contenus ne sont pas stockés en base de données mais sous forme de fichiers écrits avec le langage [markdown](https://daringfireball.net/projects/markdown/) . Ces fichiers markdown seront ensuite compilés en fichiers html.

Comme toute nodejs application, plusieurs commandes existent et permettent de créer une page, un post, une catégorie, générer des fichiers statiques, déployer sur un serveur : 
- git
- heroku
- netlify
- rsyncSFTP
- SFTP.

Si on veut changer de plateforme - par exemple de wordpress, jekyll, octopress , joomla vers hexo ou d'importer un flux rss, celà est également possible.

# Installation

## Prérequis

Installons nodejs si ce n'est pas dejà fait.

- nodejs

Si ce n'est pas encore fait, l'installation est très simple. Sur Windows, il faut aller sur https://nodejs.org/en/ et télécharger l'installeur

![](/images/nodejs-blog-powered-hexo_images/bbb6e3ea.png)

On vérifie ensuite la bonne installation de node en tapant ceci dans la console :

```
node -v
v12.4.0
```

## Installation de hexo

On installe **hexo-cli** en global

```
npm i hexo-cli -g
```

Retour de la console  

```
+ hexo-cli@2.0.0
```

On crée une application hexo dans un dossier blognodejs

```
hexo init blognodejs
```

On va dans ce dossier et on installe les dépendances.

```
cd blognodejs/
npm install
```

On veut tout de suite générer les fichiers statiques (.html, css, javascript) dans le dossier public. Ce sont les contenus de ce dossier qu’il faudra envoyer dans un espace FTP pour que le blog soit accessible via un URL. Il suffit de lancer cette commande.
```
hexo generate
```

Si on voulait générer automatiquement les fichiers **.html** après leur création/modification - on ajoute le paramètre **--watch**

```
hexo generate --watch
```

Avant de déployer le blog sur un serveur de production, observons d'abord le rendu en local. 

hexo possède un serveur nous permettant de voir cela facilement. Il suffit alors de lancer: 
```
hexo server --draft --open
```

Le paramètre :

- **--draft** permet d'afficher les brouillons (car ils sont cachés par défaut)  
- **--open** lance le navigateur et ouvre l'adresse http://localhost:4000 lorsque le serveur est prêt

Si le port 4000 est déjà utilisé par une autre instance, on peut choisir un autre port en passant le paramètre **-p** comme ceci
```
hexo server -p 4500
INFO  Start processing                                                          
INFO  Hexo is running at http://localhost:4500 . Press Ctrl+C to stop.
```

Pour afficher la liste des URLs existantes.
```
hexo list page
```

Voici le retour de cette commande

```
INFO  Start processing                                                          
Date        Title          Path                                                 
2019-06-21  About me       about/me.md                                          
2019-06-21  Bonjour        bonjour.md                                           
2019-06-21  Bonjour Miary  hello/miary.md
```

## Un petit aperçu du blog.

![](/images/nodejs-blog-powered-hexo_images/ee6dec68.jpeg)

On veut maintenant afficher un menu dynamique sur toutes les pages du blog.

Créons un dossier.

```
mkdir -p source/_data
```

Et ajoutons un fichier menu.yml dans ce dossier.
```
touch source/_data/menu.yml
```
qui contiendra le code suivant.
```
Accueil: /
Photos: /photos/
Articles: /archives/
```


- A gauche *(du ":")* : le titre du menu
- A droite *(du ":")* : l'URL de destination


Affichons ces menus sur toutes les pages. Pour cela, modifions le fichier /themes/landscape/layout/_partial/header.ejs et ajoutons
```
<% for (var link in site.data.menu) { %>
<a href="<%= site.data.menu[link] %>"> <%= link %> </a>
<% } %>
```

Voici le résultat

![](/images/nodejs-blog-powered-hexo_images/725e755a.jpeg)

### Plugins
---

Comme dans wordpress, il est possible avec hexo d'installer des [plugins](https://hexo.io/plugins/) pour ajouter des fonctionnalités utiles à notre besoin.

Nous aurions besoin des plugins suivants dans un premier temps :

- hexo-browsersync
- hexo-lazyload-image
- hexo-wordcount 

### hexo-browsersync

Parce qu'on n’a pas envie de rafraîchir la page à chaque fois qu’on fait une modification, on va laisser au navigateur de faire cela. Il nous faut installer le package suivant.
```
npm install hexo-browsersync --save
```

Arrêter à nouveau le serveur en faisant (**CTRL+C**) puis lancer le à nouveau via la commande suivante :

```
hexo server
```


## hexo-lazyload-image

Permet de présenter une icône de chargement (loading) avant l'affichage réel d'une image.

On installe 

```
npm install hexo-lazyload-image --save
```

Dans le fichier /_config.yml, on ajoute les paramètres du plugin

```
lazyload:
  enable: true
  onlypost: false
  loadingImg: # eg ./images/loading.gif
```

Il suffit ensuite d'arrêter à nouveau le serveur en faisant (**CTRL+C**) puis lancer le à nouveau via la commande suivante :

```
hexo server
```

Et à partir de là, une icône de chargement s'affiche avant de présenter l'image.

![](/images/loader.gif)

## hexo-wordcount

Permet d'ajouter des informations utiles à un post par exemple : le nombre de mots, le temps de lecture estimé.

On installe 

```
npm install hexo-wordcount --save
```

### Deploiement

Nous avons terminé la rédaction de notre premier post. Il est temps d'envoyer cela sur un serveur car l'article a besoin d'être lu assez rapidement.

Il nous faut faire quelques modifications avant de lancer le déploiement.

Dans notre cas, nous allons publier (`git push`) le code source sur [Gitlab](https://gitlab.com) . Ensuite un webhook communiquera avec notre serveur pour tirer (`git pull`) les commits. 

Installons le plugin **hexo-deployer-git** permettant de faire un déploiement via git

```
npm install hexo-deployer-git --save
```

Modifions le fichier /_config.yml et ajoutons ceci

```
deploy:
  type: git
  repo: git@gitlab.com:miary/hexo.git
  branch: prod 
  message: # messg laisser vide - par défaut "Site updated: {{ now('YYYY-MM-DD HH:mm:ss') }}"
```

lancons ensuite le deploiement

```
hexo clean && hexo deploy
```

Retour

```
INFO  Deleted database.
INFO  Deleted public folder.
INFO  Start processing
INFO  Files loaded in 575 ms
INFO  Generated: index.html
INFO  Generated: archives/index.html
...
INFO  17 files generated in 1.39 s
```
---
### * Bonus
---

#### Ecrire en markdown

Comme précisé plus haut, les pages doivent respecter le format markdown. Il n'est pas facile au début d'apprendre ce langage bien qu'il soit pratique et simple lorsqu'on prend l'habitude.

Un outil gratuit en ligne wysiwyg (What You See Is What You Get) existe et permet de nous aider à écrire du markdown. Cet outil se nomme **[stackedit](https://stackedit.io/)**  .

#### Plugins markdown

J'utilise principalement PHPSTORM comme IDE. J'ai ajouté 2 plugins qui m'aident bien à écrire du markdown:

- [Markdown Navigator](https://github.com/vsch/idea-multimarkdown) :
Editeur de markdown plus complet que l'éditeur par défaut proposé par le logiciel. Des barres d'outils supplémentaires permettent de : ajouter des liens, tableaux, listes à puces...
- [Paste Image To Markdown](https://github.com/holgerbrandl/pasteimages) 
Permet d'ajouter une image copiée  et d'enregistrer cette image dans un dossier spécifique.

#### Bookmarks et Webographie

- [Awesome hexo](https://github.com/hexojs/awesome-hexo)
- [Plugins hexo](https://hexo.io/plugins/)
- [Themes awesome hexo](https://github.com/devjin0617/awesome-hexo-theme)
- [Backend hexo-admin](https://github.com/jaredly/hexo-admin)
- [hexo-tag-fontawesome](https://github.com/akarzim/hexo-tag-fontawesome)

#### Mise à jour du 27/06/2019

##### Logiciels

Voici une liste de logiciels **gratuits** pour écrire/et ou lire du markdown :

- [typora](https://typora.io/) - développé avec **Electronjs** - fonctionne sous windows, linux et macOSX (en version beta)
- [boostnote](https://boostnote.io/) - fonctionne sous windows, linux et macOSX 
- [markdownpad](http://markdownpad.com/) - fonctionne sur windows 
- [markdownedit](https://markdownedit.com/) - fonctionne sur windows

[Mise à jour] 28 juin 2019

### Quelques commandes `hexo` utiles

- Créer une page

  ```
  hexo new page "Bonjour, bienvenue sur ma premiere page !"
  ```

  Le fichier suivant sera créé

  **source/Bonjour-bienvenue-sur-ma-premiere-page/index.md**

  

- Créer un post

  ```
  hexo new post "Le titre de mon blog"
  ```

Le fichier  **source/_posts/Le-titre-de-mon-blog.md** sera créé

- Créer un post en spécifiant son nom  

  ```
  hexo new post je-veux-donner-nom-post
  ```

  Le fichier  **source/_posts/je-veux-donner-nom-post.md** sera créé

- Créer un brouillon

  ```
  hexo new draft "Brouillon du post"
  ```

  Le fichier **source/_drafts/Brouillon-du-post.md** sera créé 

- Publier un brouillon

  ```
  hexo publish post Brouillon-du-post
  ```

  Remarquons ici qu'il faut prendre le **nom** du fichier markdown créé dans source/_drafts/

  Cette commande déplace le fichier source/_drafts/Brouillon-du-post.md vers source/_posts/Brouillon-du-post.md



