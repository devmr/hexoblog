---
title: Hexo.io - Créer un tag personnalisé
tags:
  - hexo
  - cms
categories:
  - [hexo]
permalink: fr-hexo-create-custom-tag
date: 2019-08-04 00:40:07
---


**[Hexo.io](https://hexo.io)** est un framework nodejs permettant de créer un blog au même titre que wordpress...

Les billets et articles doivent être écrits avec le langage **markdown**. Une transformation est ensuite opérée pour convertir ce markdown en format .html, pour être interprétée ensuite dans un navigateur internet (Google Chrome, Firefox...).

{% googleads 'in-article' 'fluid' '8275635211' %}

Il existe plusieurs façons d'étendre hexo tels que : filtres, generator, helper et les tags.

Nous allons nous intéresser aux tags dans cet article.

# Tag	

Un tag est un bloc de code entouré des caractères suivants :  `{ %` et `% }` . Il est utilisé dans les billets ou les articles. Il permet d'ajouter des bouts de codes de façon simple.

Par exemple un tag`fontawesome` permet d'ajouter une icône .

Le code suivant : 

```
{% fa refresh spin %}
```

Permet d'afficher l'icône : 

{% fa refresh spin %}

Nous allons créer un tag `googleads` pour afficher un code Adsense. Comme ce blog utilise **Google Adsense** pour afficher des petites publicités ;) cela tombe bien.

Il nous sera très facile ensuite d'ajouter une publicité à l'endroit où l'on veut dans un billet.

## Manipulations

Créons un fichier **google-adsense.js** dans un dossier "scripts" de notre thème. Notre thème s'appelle **miary** . Il nous faut donc créer le fichier `/themes/miary/scripts/google-adsense.js`

Voici le contenu par défaut de ce fichier

```
hexo.extend.tag.register('googleads', function(args) {
	// Code here...
});
```

- **args** : est un tableau *array* de tous les arguments passés au tag.

Les différentes options d'un tag peuvent être consultées ici: https://hexo.io/api/tag.html 

Nous allons ajouter le code suivant dans la fonction

{% gist 8e9c4c36d2865dcae41224f719612cc6  hexo_custom_tag_googleads.js %}

Le tag peut être ensuite utilisé comme ceci dans un billet

```
{% googleads 'in-article' 'fluid' '8275635211' %}
```

Et voila, on a notre publicité affichée de façon flexible.

{% googleads 'in-article' 'fluid' '8275635211' %}