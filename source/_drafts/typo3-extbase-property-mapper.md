---
title: TYPO3 - Remplir un "SELECT2" avec extbase
tags:
  - typo3
  - extbase
categories:
  - [TYPO3]
permalink: typo3-extbase-property-mapper-fr
date: 2019-08-22 20:40:07
---

# SELECT2

**[select2](https://select2.org/)** est une librairie jQuery qui permet de remplacer  un `&lt;SELECT&gt;&lt;/SELECT&gt;` en liste déroulante éditable. Grâce à cette librairie, il sera possible de chercher directement une valeur dans la liste au lieu de dérouler la liste pour choisir la valeur souhaitée.

Ce remplacement se fait comme ceci

{% gist 66f1b3b110e0d79641d1b5a0380e7a18 select2-basique.html %}

{% googleads 'in-article' 'fluid' '8275635211' %}

Cette librairie possède plusieurs possibilités. 

pb: property mapper allow impossible

select multiple et select2

select multiple alimenté dynamiquement par select2

cause: absence values dans seet multiple car alimenté via select2 en ajax

afficher model domain + TCA

afficher controller

ext_loclconf >> eid_include

code eid

template html

typoscript includejslib select2

modif methods createAction et updateAction  dans controller

