---
title: typo3-composer-autoload-vs-old-autoload
tags:
  - typo3
categories:
  - [Tips-Tricks]
  - [TYPO3]
permalink: typo3-composer-autoload-fr
date: 2019-08-17 20:40:07
---

# Problématique

Expliquer autoload

Pour charger automatiquement les classes, TYPO3 utilise l'autoload de composer depuis la version 8.

## Solution

Modif multisite typo3conf/sites/portal/config.yaml

Modifions composer.json puis ajoutons dans la section

```
"autoload": {
		"psr-4": {
		"GeorgRinger\\News\\": "typo3conf/ext/news/Classes/"
		}
}
```

Tout ce qu'il y a dans 

typo3conf/autoload/autoload_psr4.php

typo3conf/autoload/autoload_classmap.php

Donner un exemple de code dans ces 2 scripts et montrer le résultat dans composer

Et ensuite lancer la commande

```
composer dumpautoload
```

Vider les caches de TYPO3 manuellement

```
rm -rf typo3temp/var/cache/*
```

