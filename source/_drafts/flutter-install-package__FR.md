---
title: Flutter - ajouter/installer un package
tags:
  - flutter
categories:
  - [Flutter] 
permalink: flutter-install-package
date: 2019-11-05 20:40:07

---

# Objectif

Création d'une application mobile avec Flutter. 

Le cahier des charges de cette application :

- Pouvoir envoyer une image depuis le téléphone vers un serveur. Il est possible de récupérer l'image :
  - En la sélectionnant depuis le telephone
  - Depuis une image affichée dans une application externe (facebook par exemple.)
- Il sera possible de modifier/cadrer l'image autant de fois que l'on souhaite avant d'envoyer l'image souhaitée sur le serveur.

## Creation d'une application vide.

Allons dans un dossier (exemple : `C:\sites\flutter` ) en ligne de commande et executons la commande suivante pour créer une application.

```
flutter create app
```

Une fois l'application créée dans le dossier `C:\sites\flutter\app`, il est temps de le lancer sur le téléphone. Il suffit de faire alors :

```
flutter run
```

et d'attendre que l'application se lance.

# Installation de package utiles

Package à ajouter :

-  **receive_sharing_intent** : https://pub.dev/packages/receive_sharing_intent 

## Package receive_sharing_intent

Utilité du package : reception des informations (média venant d'autres applications - images facebook par exemple)

On modifie le fichier `pubspec.yaml` et dans dependencies, on ajoute

```yaml
dependencies:
  receive_sharing_intent: ^1.3.1+1
```

On télécharge le package en faisant

```
flutter pub get
```

On modifie ensuite app/src/main/AndroidManifest.xml

{% gist 76a4cb5bcd4188c3e8f1099b8bcd21e2 AndroidManifest.xml %}





