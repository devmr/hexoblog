
/**
 * googleads tag
 * <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
 <ins class="adsbygoogle"
 style="display:block; text-align:center;"
 data-ad-layout="in-article"
 data-ad-format="fluid"
 data-ad-client="ca-pub-8414180784657235"
 data-ad-slot="8275635211"></ins>
 <script>
 (adsbygoogle = window.adsbygoogle || []).push({});
 </script>
 *
 * Syntax:
 *   {% googleads [layout] [format] [data-ad-slot] %}
 *   {% googleads false false [data-ad-slot] %}
 */
hexo.extend.tag.register('googleads', function(args) {

    if (args.length !== 3) {
        return '';
    }

    let layout = args[0] || 'in-article';
    let format = args[1] || 'fluid';
    let slot = args[2];

    let sHtml = '<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>' + "\n";
    sHtml += '<ins class="adsbygoogle"' + "\n";
    sHtml += 'style="display:block; text-align:center;"' + "\n";
    sHtml += 'data-ad-layout="' + layout + '"' + "\n";
    sHtml += 'data-ad-format="' + format + '"' + "\n";
    sHtml += 'data-ad-client="ca-pub-8414180784657235"' + "\n";
    sHtml += 'data-ad-slot="' + slot + '"' + "\n";
    sHtml += '></ins>' + "\n";
    sHtml += '<script>' + "\n";
    sHtml += '(adsbygoogle = window.adsbygoogle || []).push({});' + "\n";
    sHtml += '</script>' + "\n";


    return sHtml;

});